
# Bautadni

Bautadni is a versatile text browser designed to assist users in analyzing text through customizable word relationships and interactive highlighting.

## Step-by-step User Documentation

### Basic navigation & terminology

The interface of Bautadni comprises two **Buffer Rings**, each housing a set of **Buffers** that can be navigated through horizontal swiping. *Buffers* within a single *Buffer Rings* cycle continuously.

To manage *buffers*, users can employ either the back gesture or the back button, which reveals an overlay menu offering the following options:

- Open Workspace browser
- Open Settings - currently only to access data browsers
- Move the current *buffer* to the other *Buffer Ring*
- Toggle Pin/Unpin behavior for the current *buffer*
    - In an unpinned state, the *buffer* closes as soon as the user swipes/opens another *buffer* over it
- Close the current *buffer*

### Browsers

Bautadni manages various data, all of which can be manipulated in data browsers: users can long-tap a data entry from a list to access additional options, such as editing and removal.

#### Books

Books serve as the primary sources of text for Bautadni. Each book may be associated with a list of languages contained in it. When creating a book, the language list provides a selection hint for later use. Once a book is added, users can open it into a *buffer* by single-tapping the book item and specifying the language.

Language becomes particularly significant when interacting with words.

##### Word selection

In a *buffer* with an open book, users can select a word by double/long tapping the word and choosing the `Select` option from the menu that pops up. This action highlights up to two words and reveals a **button bar** for additional options.
Repeated selection **unselects** the word.

#### Words

Every word is identified by its textual form and the language of the book it originates from. Single-tapping a word in the word browser opens the word's note *buffer*. Within the word's note *buffer*, users can view its relations, language, and set a custom textual note.

Users can also summon the note *buffer* when a single word is selected in the book *buffer* by using the corresponding button from the *button bar*.

#### Relations

Relations are user-defined data for connecting pairs of words (e.g. `Synonym`, `Translation`). When creating a relation, users specify a name, color, and transitivity.

When one word is selected in the book *buffer*, the *button bar* includes buttons that highlight all words related to the selected word by that relation. Similarly, when two words are selected, the *button bar* contains additional buttons to create a relation between them.

#### Formulae

Formulae enable the combination of multiple relations and the highlighting of all relevant words. After creating and opening a Formula, users can add relations for evaluation. For non-transitive relations, users can specify the desired depth of relation steps to be highlighted.

To apply a formula, users select a word and tap the corresponding formula button in the *button bar*. The application of a formula involves applying the relations in sequence, and all resulting words are highlighted according to the formula's color.

##### Example

If a formula includes both the synonym and translation relations, applied in that order, Bautadni highlights all the translations of the word's synonyms. These translations are based on the relationships set earlier using the relation buttons.

## Technical Documentation

### Persistence

- Persistence is achieved by putting most data info Room database.
- For smaller settings we use DataStore.

### Format conversion

We support loading PDFs and images as text, for this conversion we following libraries:
- PDF - PDFBox https://pdfbox.apache.org/ via https://github.com/TomRoush/PdfBox-Android
- image - ML Kit https://developers.google.com/ml-kit/

### Compose

Most of the app is written in Compose, for which we used following libraries:
- colorpicker - https://github.com/skydoves/colorpicker-compose
- icon pack - https://tabler.io/ via https://github.com/DevSrSouza/compose-icons
