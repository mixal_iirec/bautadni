package com.bautadni

import android.text.style.BackgroundColorSpan
import android.text.style.StyleSpan
import androidx.activity.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bautadni.WordSelectionViewModel.Companion.defaultPolicy
import com.bautadni.data.lang.WordData
import com.bautadni.buffers.pager.HighlightWord

class BauWord(var text: String, var lang: String, val buffRingUid: Int) {
    override fun equals(other: Any?): Boolean {
        if (other == null) {
            return false
        } else if (other !is BauWord) {
            return false
        }
        return text == other.text && lang == other.lang
    }

    override fun hashCode(): Int {
        var result = text.hashCode()
        result = 31 * result + lang.hashCode()
        return result
    }
}

class BufferRingWordSelection(
    private val idxMap: MutableMap<Int, Int>,
    private val buffRingMap: MutableMap<Int, Int>,
    val selected: MutableList<BauWord?>,
    val unselected: MutableList<BauWord?>
) {
    // buffRingMap: selection buffer ring id -> index in word selection
    // idxMap: index -> buffer ring id in word selection

    constructor() : this(mutableMapOf(), mutableMapOf(), mutableListOf(), mutableListOf())

    fun withUpdatedSelection(
        sel: MutableList<BauWord?>, unselected: MutableList<BauWord?>
    ): BufferRingWordSelection {
        sel.forEachIndexed { index, s ->
            if (s == null) {
                idxMap.remove(index)
            }
        }
        return BufferRingWordSelection(idxMap, buffRingMap, sel, unselected)
    }

    fun getBuffRingId(idx: Int): Int {
        return idxMap.getOrDefault(idx, -1)
    }

    fun setBuffRingId(idx: Int, buffRingId: Int) {
        idxMap[idx] = buffRingId
    }

    fun removeBuffRingId(idx: Int) {
        idxMap.remove(idx)
    }

    fun getIdx(buffRingId: Int): Int {
        return buffRingMap.getOrDefault(buffRingId, -1)
    }

    fun setIdx(buffRingId: Int, idx: Int) {
        buffRingMap[buffRingId] = idx
    }

    fun removeIdx(buffRingId: Int) {
        buffRingMap.remove(buffRingId)
    }

    fun hasIdxAssigned(buffRingId: Int): Boolean {
        return buffRingMap.containsKey(buffRingId)
    }

    fun containsIdx(idx: Int): Boolean {
        return buffRingMap.containsValue(idx)
    }
}

interface WordSelectionPolicy {
    fun onWordSelected(
        current: BufferRingWordSelection, word: BauWord, buffRingId: Int
    ): BufferRingWordSelection

    fun emptyWordSelection(): BufferRingWordSelection

    fun onReset(current: BufferRingWordSelection, buffRingId: Int): BufferRingWordSelection
}

class WordSelectionState(val policy: WordSelectionPolicy, var selection: BufferRingWordSelection) {
    fun updatedSelectionState(word: String, buffRingId: Int, language: String): WordSelectionState {
        val newSelection =
            policy.onWordSelected(selection, BauWord(word, language, buffRingId), buffRingId)
        return WordSelectionState(policy, newSelection)
    }

    fun updatedSelectionState(newPolicy: WordSelectionPolicy): WordSelectionState {
        return WordSelectionState(newPolicy, newPolicy.emptyWordSelection())
    }
}

class WordSelectionViewModel : ViewModel() {


    var state: MutableLiveData<WordSelectionState> =
        MutableLiveData(WordSelectionState(defaultPolicy, defaultPolicy.emptyWordSelection()))
    var highlightedWords: MutableLiveData<List<HighlightWord>> = MutableLiveData(listOf())

    // workaround make it nearly impossible to demand 2 relation highlights at once
    // holds the name of currently highlighted relation
    var relatedHighlightLock: MutableLiveData<String> = MutableLiveData("")

    fun getSelectedWords(): List<Pair<WordData, Int>> {
        return state.value?.let {
            it.selection.selected.filterNotNull()
                .map { w -> Pair(WordData(0, w.text, w.lang), w.buffRingUid) }
        } ?: listOf()
    }

    fun unhighlightAll() {
        highlightedWords.value = listOf()
    }

    fun addHighlight(words: List<HighlightWord>) {
        var highlighted = getSelectedWords().map { HighlightWord(BauWord(it.first.word, it.first.lang, it.second), WordSelectorManager.defaultStyle) }
        // remap highlighting of already-highlighted words
        highlighted = highlighted.map {
            val hl = words.find { rw -> rw.bauWord == it.bauWord }?.style
            if (hl != null) {
                HighlightWord(it.bauWord, hl)
            } else {
                it
            }
        }
        // add words which are not highlighted yet
        val addition = words.filter {
            highlighted.find { rw -> rw.bauWord == it.bauWord } == null
        }
        highlightedWords.value = highlighted + addition
    }

    fun changePolicy(newPolicy: WordSelectionPolicy) {
        state.value = state.value?.updatedSelectionState(newPolicy)
    }

    companion object {
        fun equal(w: BauWord?, w2: BauWord?): Boolean {
            return w?.equals(w2) ?: (w2 == null)
        }

        val singleWordSelectionPolicy = object : WordSelectionPolicy {
            override fun onWordSelected(
                current: BufferRingWordSelection, word: BauWord, buffRingId: Int
            ): BufferRingWordSelection {
                val selected = current.selected
                val unselected = current.unselected
                if (equal(selected[0], word)) {
                    // reselection
                    unselected.add(selected[0])
                    selected[0] = null
                    current.removeBuffRingId(0)

                } else {
                    if (selected[0] != null) {
                        current.removeBuffRingId(0)
                        unselected.add(selected[0])
                    }
                    current.setBuffRingId(0, buffRingId)
                    selected[0] = word
                }
                return current.withUpdatedSelection(selected, unselected)
            }

            override fun emptyWordSelection(): BufferRingWordSelection {
                val retval = BufferRingWordSelection()
                retval.selected.add(null)
                return retval
            }

            override fun onReset(
                current: BufferRingWordSelection, buffRingId: Int
            ): BufferRingWordSelection {
                val selected = current.selected
                val unselected = current.unselected
                if (buffRingId == current.getBuffRingId(0)) {
                    unselected.add(selected[0])
                    selected[0] = null
                    current.removeBuffRingId(0)
                }
                return current.withUpdatedSelection(selected, unselected)
            }
        }

        val uniqueBuffRingPairSelectionPolicy =
            // selects a pair of words from a different bufferrring id
            object : WordSelectionPolicy {
                override fun onWordSelected(
                    current: BufferRingWordSelection,
                    word: BauWord,
                    buffRingId: Int
                ): BufferRingWordSelection {
                    assert(current.selected.size == 2)
                    val buffRingIdx = if (current.hasIdxAssigned(buffRingId)) {
                        current.getIdx(buffRingId)
                    } else if (current.containsIdx(0)) {
                        1
                    } else {
                        0
                    }
                    current.setIdx(buffRingId, buffRingIdx)

                    val selected = current.selected
                    val unselected = mutableListOf<BauWord?>()
                    val currentSelectionForBuffRing = selected[buffRingIdx]
                    if (equal(currentSelectionForBuffRing, word)) {
                        selected[buffRingIdx] = null
                        unselected.add(currentSelectionForBuffRing)
                    } else {
                        if (currentSelectionForBuffRing != null) {
                            unselected.add(currentSelectionForBuffRing)
                        }
                        selected[buffRingIdx] = word
                    }
                    return current.withUpdatedSelection(selected, unselected)
                }

                override fun emptyWordSelection(): BufferRingWordSelection {
                    val retval = BufferRingWordSelection()
                    retval.selected.add(null)
                    retval.selected.add(null)
                    return retval
                }

                override fun onReset(
                    current: BufferRingWordSelection, buffRingId: Int
                ): BufferRingWordSelection {
                    val idx = current.getIdx(buffRingId)
                    val selected = current.selected
                    val unselected = current.unselected
                    if (idx != -1) {
                        unselected.add(selected[idx])
                        selected[idx] = null
                        current.removeIdx(buffRingId)
                    }
                    return current.withUpdatedSelection(selected, unselected)
                }
            }

        val pairSelectionPolicy = object : WordSelectionPolicy {
            override fun onWordSelected(
                current: BufferRingWordSelection, word: BauWord, buffRingId: Int
            ): BufferRingWordSelection {
                assert(current.selected.size == 2)
                val selected = current.selected
                val unselected = current.unselected
                if (equal(selected[0], word)) {
                    val tmp = emptyWordSelection().apply {
                        this.selected[0] = selected[1]
                        this.unselected.add(selected[0])
                    }
                    return current.withUpdatedSelection(tmp.selected, tmp.unselected)
                } else if (equal(selected[1], word)) {
                    val tmp = emptyWordSelection().apply {
                        this.selected[0] = selected[0]
                        this.unselected.add(selected[1])
                    }
                    return current.withUpdatedSelection(tmp.selected, tmp.unselected)
                }

                if (selected[0] == null) {
                    selected[0] = word
                    current.setBuffRingId(0, buffRingId)
                } else if (selected[1] == null) {
                    selected[1] = word
                    current.setBuffRingId(1, buffRingId)
                } else {
                    unselected.add(selected[0])
                    selected[0] = selected[1]
                    current.setBuffRingId(0, current.getBuffRingId(1))
                    selected[1] = word
                    current.setBuffRingId(1, buffRingId)
                }
                return current.withUpdatedSelection(selected, unselected)
            }

            override fun emptyWordSelection(): BufferRingWordSelection {
                val retval = BufferRingWordSelection()
                retval.selected.add(null)
                retval.selected.add(null)
                return retval
            }

            override fun onReset(
                current: BufferRingWordSelection, buffRingId: Int
            ): BufferRingWordSelection {
                val selected = current.selected
                val unselected = current.unselected
                val indicesToClear =
                    List(selected.size) { index -> current.getBuffRingId(index) }.mapIndexed { index, id ->
                        if (id == buffRingId) {
                            index
                        } else {
                            -1
                        }
                    }.filter { it != -1 }
                indicesToClear.forEach {
                    current.removeBuffRingId(it)
                    unselected.add(selected[it])
                    selected[it] = null
                }

                if (selected[0] == null && selected[1] != null) {
                    // shift 1 to 0 because 0 was unselected
                    current.selected[0] = current.selected[1]
                    current.selected[1] = null
                    current.setBuffRingId(0, current.getBuffRingId(1))
                    current.removeBuffRingId(1)
                }

                return current.withUpdatedSelection(selected, current.unselected)
            }
        }

        val pairSelectionPolicyPreferDifferentBuffers = object : WordSelectionPolicy {
            override fun onWordSelected(
                current: BufferRingWordSelection, word: BauWord, buffRingId: Int
            ): BufferRingWordSelection {
                assert(current.selected.size == 2)
                assert(!(current.selected[0] == null && current.selected[1] != null))
                val id0 = current.getBuffRingId(0)
                val id1 = current.getBuffRingId(1)
                val unselected = mutableListOf<BauWord?>()

                // reselection
                if (equal(current.selected[0], word) && id0 == buffRingId) {
                    current.selected[0] = null
                    current.removeBuffRingId(0)
                    unselected += word

                    // shift
                    if (current.selected[1] != null) {
                        // shift 1 to 0 because 0 is evicted
                        current.selected[0] = current.selected[1]
                        current.selected[1] = null
                        current.setBuffRingId(0, current.getBuffRingId(1))
                        current.removeBuffRingId(1)
                    }

                    return current.withUpdatedSelection(current.selected, unselected)
                } else if (equal(current.selected[1], word) && id1 == buffRingId) {
                    current.selected[1] = null
                    current.removeBuffRingId(1)
                    unselected += word
                    return current.withUpdatedSelection(current.selected, unselected)
                }

                // no reselection
                var evictionIdx = if (id0 == -1) {
                    0
                } else if (id1 == -1) {
                    1
                } else {
                    // evict
                    if (id0 == buffRingId && id1 != buffRingId) {
                        0
                    } else if (id1 == buffRingId && id0 != buffRingId) {
                        1
                    } else {
                        0 // evict the first one - the oldest selection (see assert)
                    }
                }

                if (evictionIdx == 0 && current.selected[1] != null) {
                    // shift - same as above...
                    // shift 1 to 0 because 0 is evicted
                    current.selected[0] = current.selected[1]
                    current.selected[1] = null
                    current.setBuffRingId(0, current.getBuffRingId(1))
                    current.removeBuffRingId(1)
                    evictionIdx = 1
                }

                // shifted -> always insert into slot 1
                current.setBuffRingId(evictionIdx, buffRingId)
                current.selected[evictionIdx] = word

                return current.withUpdatedSelection(current.selected, current.unselected)
            }

            override fun emptyWordSelection(): BufferRingWordSelection {
                val retval = BufferRingWordSelection()
                retval.selected.add(null)
                retval.selected.add(null)
                return retval
            }

            override fun onReset(
                current: BufferRingWordSelection, buffRingId: Int
            ): BufferRingWordSelection {
                val selected = current.selected
                val unselected = current.unselected
                val indicesToClear =
                    List(selected.size) { index -> current.getBuffRingId(index) }.mapIndexed { index, id ->
                        if (id == buffRingId) {
                            index
                        } else {
                            -1
                        }
                    }.filter { it != -1 }
                indicesToClear.forEach {
                    current.removeBuffRingId(it)
                    unselected.add(selected[it])
                    selected[it] = null
                }

                if (selected[0] == null && selected[1] != null) {
                    // shift 1 to 0 because 0 was unselected
                    current.selected[0] = current.selected[1]
                    current.selected[1] = null
                    current.setBuffRingId(0, current.getBuffRingId(1))
                    current.removeBuffRingId(1)
                }

                return current.withUpdatedSelection(selected, current.unselected)
            }
        }

        val defaultPolicy = pairSelectionPolicy
    }

}

class HighlightStyle(val typeface: Int, val color: Int)

class WordSelectionStyle(
    val styleSpanGenerator: () -> StyleSpan, val highlightSpanGenerator: () -> BackgroundColorSpan
)

class WordSelectorManager(activity: BufferActivity) {
    private val viewModel: WordSelectionViewModel by activity.viewModels()

    companion object {
        val defaultStyle =
            HighlightStyle(android.graphics.Typeface.BOLD, android.graphics.Color.YELLOW)

        fun getStyle(style: HighlightStyle): WordSelectionStyle {
            return WordSelectionStyle({ StyleSpan(style.typeface) },
                { BackgroundColorSpan(style.color) })
        }
    }

    fun getSelectionLiveData(): MutableLiveData<WordSelectionState> {
        return viewModel.state
    }

    fun resetSelection(buffRingId: Int) {
        val selection = viewModel.state.value
        val policy = selection?.policy ?: defaultPolicy
        val resetSelection =
            policy.onReset(selection?.selection ?: policy.emptyWordSelection(), buffRingId)
        viewModel.state.value = WordSelectionState(selection?.policy ?: policy, resetSelection)
    }
}
