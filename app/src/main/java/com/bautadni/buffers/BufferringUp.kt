package com.bautadni.buffers

import com.bautadni.BufferActivityUp
import kotlinx.coroutines.flow.Flow

enum class Where {
    HERE,
    OTHER,
}

interface BufferringUp {
    val activity: BufferActivityUp
    val thisRing: Int
    val otherRing: Int
    fun bufferCount(): Flow<Int>

    fun open(state: BufferState, where: Where)

    fun move()

    /* Close HERE. */
    fun close()
}