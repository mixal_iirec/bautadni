package com.bautadni.buffers

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.add
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.windowInsetsPadding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconToggleButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewModelScope
import com.bautadni.BufferActivity
import com.bautadni.R
import com.bautadni.data.AppDatabase
import com.bautadni.data.BufferData
import com.bautadni.data.BufferType
import com.bautadni.buffers.formula.FormulaBufferState
import com.bautadni.buffers.pager.PagerBufferState
import com.bautadni.buffers.settings.SettingsBufferState
import com.bautadni.buffers.word.WordBufferState
import com.bautadni.buffers.search.SearchBufferState
import com.bautadni.ui.theme.BautadniTheme
import compose.icons.TablerIcons
import compose.icons.tablericons.ArrowForwardUp
import compose.icons.tablericons.Pinned
import compose.icons.tablericons.PinnedOff
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class BufferState(
    /* Data should uniquely identify this buffer. */
    val data: BufferData
) {
    var temporary by mutableStateOf(false)
    lateinit var bufferring: BufferringUp

    companion object {
        fun create(data: BufferData): BufferState {
            return when (data.type) {
                BufferType.Pager -> PagerBufferState(data)
                BufferType.FormulaEditor -> FormulaBufferState(data)
                BufferType.Word -> WordBufferState(data)
                BufferType.Settings -> SettingsBufferState(data)
                BufferType.SearchBook, BufferType.SearchWord, BufferType.SearchRelation, BufferType.SearchFormula, BufferType.SearchWorkSpace
                    -> SearchBufferState(data)
                BufferType.Invalid -> SearchBufferState(BufferData(0, BufferType.SearchBook, 0))
            }
        }
    }

    fun construct(): Buffer {
        val buffer = bufferConstructor()
        buffer.temporaryStateStorage = this
        return buffer
    }
    abstract fun bufferConstructor(): Buffer
}

open class BufferViewModel: ViewModel() {
    /* State type must always correspond to Buffer type. */
    lateinit var state: BufferState

    suspend fun changeTypeUid (uid: Int) {
        if (state.data.typeUid != uid) {
            state.data.typeUid = uid
            viewModelScope.launch(context = Dispatchers.IO) {
                val database = AppDatabase.getInstance()
                database.buffer().insert(state.data).toInt()
            }
        }
    }


    fun onCreate(_state: BufferState) {
        Log.d("viewModel", "create $this")
        state = _state
        if (state.data.uid == 0 && state.data.type != BufferType.SearchWorkSpace) {
            viewModelScope.launch(context = Dispatchers.IO){
                val database = AppDatabase.getInstance()
                state.data.uid = database.buffer().insert(state.data).toInt()
                withContext(context = Dispatchers.Main) {
                    state.bufferring.activity.saveWorkspace()
                }
            }
        }
    }
}

abstract class Buffer: Fragment(R.layout.compose_buffer) {
    open val viewModel: BufferViewModel by viewModels()
    val state get() = viewModel.state
    var temporaryStateStorage: BufferState? = null


    var inset by mutableStateOf(WindowInsets(0,0,0,0))

    private fun updateInset (insets: WindowInsets, overlay: Boolean): WindowInsets {
        val atTop = if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            state.bufferring.thisRing == 0
        } else {
            true
        }

        context?.let { context ->
            Log.d("insets", "${insets.getTop(Density(context))}")
        }
        val insetBar = if (atTop) insets else WindowInsets(0,0,0,0)

        val activity = activity as BufferActivity
        val toolbarHeight = activity.toPxl(48f)
        var insetTop = insetBar
        if (overlay) {
            insetTop = insetBar.add(WindowInsets(0, toolbarHeight, 0, 0))
        }
        if (inset != insetTop) {
            inset = insetTop
        }
        return insetBar
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        temporaryStateStorage?.let {
            viewModel.onCreate(it)
        }
        temporaryStateStorage = null
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun OverlayCompose() {
        val activity = activity as BufferActivity
        val inset = updateInset(TopAppBarDefaults.windowInsets, activity.overlay)


        if (!activity.overlay) return
        Log.d("compose", "update ${(state.bufferring as Bufferring).pager}")

        val self = this
        val open = { type: BufferType, uid: Int, tmp: Boolean -> self.state.bufferring.open(
            BufferState.create(BufferData(uid, type, 0)).also{it.temporary = tmp}, Where.HERE)
        }
        val buttonModifier = Modifier .windowInsetsPadding(inset)
            .padding(4.dp, 4.dp, 4.dp, 0.dp)
            .size(40.dp)
            .background(MaterialTheme.colorScheme.outlineVariant, shape = CircleShape)
            .size(48.dp)
        val enableRemove = state.bufferring.bufferCount().map {it > 1}.collectAsStateWithLifecycle(initialValue = true)
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Row {
                IconButton(
                    modifier = buttonModifier,
                    onClick = { open(BufferType.SearchWorkSpace, 1, false) }) {
                    Icon(imageVector = Icons.Filled.List, contentDescription = "Workspaces")
                }
                IconButton(modifier = buttonModifier, onClick = { open(BufferType.Settings, 0, true) }) {
                    Icon(imageVector = Icons.Filled.Settings, contentDescription = "Settings")
                }
                IconButton(modifier = buttonModifier, onClick = { self.state.bufferring.move() }, enabled = enableRemove.value) {
                    Icon(imageVector = TablerIcons.ArrowForwardUp, contentDescription = "Move")
                }
            }
            Row {
                IconToggleButton( modifier = buttonModifier, checked = state.temporary, onCheckedChange = { state.temporary = it }) {
                    if (state.temporary) {
                        Icon(imageVector = TablerIcons.PinnedOff, contentDescription = "Temporary")
                    } else {
                        Icon(imageVector = TablerIcons.Pinned, contentDescription = "Persistent")
                    }
                }
                IconButton(modifier = buttonModifier, onClick = { self.state.bufferring.close() }, enabled = enableRemove.value) {
                    Icon(imageVector = Icons.Filled.Close, contentDescription = "Close")
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<ComposeView>(R.id.overlay).setContent {
            BautadniTheme { OverlayCompose() }
        }
    }
}