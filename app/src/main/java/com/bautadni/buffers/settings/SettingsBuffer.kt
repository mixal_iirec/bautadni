package com.bautadni.buffers.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.windowInsetsPadding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import com.bautadni.R
import com.bautadni.buffers.Where
import com.bautadni.data.BufferData
import com.bautadni.data.BufferType
import com.bautadni.buffers.Buffer
import com.bautadni.buffers.BufferState
import com.bautadni.ui.theme.BautadniTheme

class SettingsBufferState(_data: BufferData): BufferState(_data) {
    override fun bufferConstructor() = SettingsBuffer()
}

class SettingsBuffer: Buffer() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.compose_buffer, container, false).apply {
            findViewById<ComposeView>(R.id.compose).setContent {
                BautadniTheme { Compose() }
            }
        }
    }

    @Composable
    fun Compose() {
        val self = this
        val openHere = { type: BufferType -> {
            self.state.bufferring.open(BufferState.create(BufferData(0, type, 0)), Where.HERE)
        }}

        LazyVerticalGrid(modifier = Modifier.windowInsetsPadding(inset), columns = GridCells.Fixed(2), verticalArrangement = Arrangement.Center) {
            items(1) {
                Button(onClick = openHere(BufferType.SearchBook), modifier=Modifier.padding(8.dp)) {
                    Text(getString(R.string.search_book_title))
                }
            }
            items(1) {
                Button(onClick = openHere(BufferType.SearchRelation), modifier=Modifier.padding(8.dp)) {
                    Text(getString(R.string.search_relation_title))
                }
            }
            items(1) {
                Button(onClick = openHere(BufferType.SearchFormula), modifier=Modifier.padding(8.dp)) {
                    Text(getString(R.string.search_formula_title))
                }
            }
            items(1) {
                Button(onClick = openHere(BufferType.SearchWord), modifier=Modifier.padding(8.dp)) {
                    Text(getString(R.string.search_word_title))
                }
            }
        }

    }
}