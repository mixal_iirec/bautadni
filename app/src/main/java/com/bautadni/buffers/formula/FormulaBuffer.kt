package com.bautadni.buffers.formula

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Transparent
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.lifecycleScope
import com.bautadni.R
import com.bautadni.data.AppDatabase
import com.bautadni.data.BufferData
import com.bautadni.data.ColorWrapper
import com.bautadni.data.lang.Formula
import com.bautadni.data.lang.FormulaData
import com.bautadni.data.lang.FormulaInnerData
import com.bautadni.data.lang.RelationData
import com.bautadni.buffers.Buffer
import com.bautadni.buffers.BufferState
import com.bautadni.ui.theme.BautadniTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FormulaBufferState(_data: BufferData): BufferState(_data) {
    override fun bufferConstructor() = FormulaBuffer()
}

class FormulaBuffer: Buffer() {
    private var formula by mutableStateOf(Formula(FormulaData(0, 0, "", ColorWrapper(Color.Green))))
    private var relations: Map<Int, RelationData> = mapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launch {
            withContext(context = Dispatchers.IO) {
                val database = AppDatabase.getInstance()
                val rel = database.relation().all()
                relations = if (rel.isNotEmpty()) {
                    rel.associateBy { it.uid }
                } else {
                    mapOf(Pair(0,
                        RelationData(0, "(no relations)", false, ColorWrapper(Color.Green))
                    ))
                }
                database.formula().byUid(state.data.typeUid)?.let {
                    val formula = Formula(it)
                    formula.load()
                    withContext(context = Dispatchers.Main) {
                        this@FormulaBuffer.formula = formula
                    }
                }
            }
            snapshotFlow { formula }.collect { it.save() }
        }
    }

    private fun addFormula() {
        val form = formula.memoryCopy()
        form.inners.add( FormulaInnerData(0, 0, 0, 1, 1) )
        formula = form
    }
    private fun remFormula() {
        val form = formula.memoryCopy()
        form.inners.removeLast()
        formula = form
    }

    private fun changeInner(idx: Int, inner: FormulaInnerData) {
        val form = formula.memoryCopy()
        form.inners[idx] = inner
        formula = form
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.compose_buffer, container, false).apply {
            findViewById<ComposeView>(R.id.compose).setContent {
                BautadniTheme { Compose() }
            }
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun Compose() {
        val circle = Modifier
            .padding(4.dp)
            .size(48.dp)
            .background(formula.data.color.c, shape = CircleShape)
            .border(1.dp, Color.Black, shape = CircleShape)
        val buttonModifier = Modifier
            .padding(4.dp)
            .size(40.dp)
            .background(MaterialTheme.colorScheme.primaryContainer, shape = CircleShape)
            .size(48.dp)

        val strToInt = {str: String ->
            var new = str.toIntOrNull()
            if (str == "") new = 0
            if (str == "∞") new = Int.MAX_VALUE
            new
        }
        val intToStr = { int: Int ->
            when (int) {
                Int.MAX_VALUE -> "∞"
                else -> int.toString()
            }
        }
        val scrollState = rememberScrollState()
        Column(modifier = Modifier.verticalScroll(scrollState)) {
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
                Text(formula.data.name, fontSize = 48.sp)

                Box(modifier = circle) {}
            }


            val fieldColors = ExposedDropdownMenuDefaults.textFieldColors(
                focusedIndicatorColor = Transparent,
                unfocusedIndicatorColor = Transparent
            )
            val height = 64.dp
            val itemWidth = 64.dp
            for (i in 0..<formula.inners.size) {
                val inner = formula.inners[i]
                val rel = relations[formula.inners[i].relKindUid]
                BoxWithConstraints {
                    val midWidth = this.maxWidth - itemWidth * 3
                    Row(
                        modifier = Modifier.fillMaxWidth().height(height).width(itemWidth).padding(4.dp),
                        horizontalArrangement = Arrangement.Absolute.SpaceEvenly
                    ) {
                        if (rel?.transitive != true) {
                            TextField(
                                intToStr(inner.minCount),
                                onValueChange = {
                                    strToInt(it)?.let { changeInner(i, inner.copy(minCount = it)) }
                                }, modifier = Modifier.width(itemWidth).height(height),
                                textStyle = TextStyle.Default.copy(fontSize = 20.sp, textAlign = TextAlign.Center),
                                keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number)
                            )
                            Text( "≤", fontSize = 40.sp, textAlign = TextAlign.Center, modifier = Modifier.height(height) )
                        }
                        var expanded by remember { mutableStateOf(false) }

                        ExposedDropdownMenuBox( expanded = expanded, onExpandedChange = { expanded = !expanded }, modifier = Modifier.width(midWidth)) {
                            TextField(
                                modifier = Modifier.menuAnchor(),
                                textStyle = TextStyle.Default.copy( fontSize = 20.sp, textAlign = TextAlign.Center ),
                                readOnly = true,
                                value = rel?.name ?: "",
                                onValueChange = {},
                                trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded) },
                                shape = RoundedCornerShape(8.dp),
                                colors = fieldColors
                            )
                            ExposedDropdownMenu(
                                expanded = expanded,
                                onDismissRequest = { expanded = false }) {
                                relations.values.forEach { rel ->
                                    DropdownMenuItem(
                                        text = { Text( rel.name, textAlign = TextAlign.Center, fontSize = 20.sp ) },
                                        onClick = {
                                            val newInner = if (rel.transitive) {
                                                inner.copy(relKindUid = rel.uid, minCount = 0, maxCount = Int.MAX_VALUE)
                                            } else {
                                                inner.copy(relKindUid = rel.uid)
                                            }
                                            changeInner(i, newInner)
                                            expanded = false
                                        },
                                        contentPadding = ExposedDropdownMenuDefaults.ItemContentPadding,
                                    )
                                }
                            }
                        }
                        if (rel?.transitive != true) {
                            Text("≤", fontSize = 40.sp, textAlign = TextAlign.Center, modifier = Modifier.height(height))
                            TextField(
                                intToStr(inner.maxCount),
                                onValueChange = {
                                    strToInt(it)?.let { changeInner(i, inner.copy(maxCount = it)) }
                                }, modifier = Modifier.width(itemWidth).height(height),
                                textStyle = TextStyle.Default.copy(fontSize = 20.sp, textAlign = TextAlign.Center),
                                keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number)
                            )
                        }
                    }
                }
            }

            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceEvenly) {
                IconButton(modifier = buttonModifier, onClick = { addFormula() } ) {
                    Icon(imageVector = Icons.Filled.Add, contentDescription = "Add")
                }
                if (formula.inners.size > 0) {
                    IconButton(modifier = buttonModifier, onClick = { remFormula() }) {
                        Icon(imageVector = Icons.Filled.Close, contentDescription = "Remove")
                    }
                }
            }
        }
    }
}