package com.bautadni.buffers.word

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.bautadni.data.lang.Formula
import com.bautadni.data.lang.RelationData

class WordRelations(wordUid: Int, private val relation: RelationData) {
    private val flowRelated = Formula.simpleRelation(relation).applyFlow(wordUid)
    @Composable
    fun Compose() {
        val words = flowRelated.collectAsStateWithLifecycle(initialValue = listOf()).value
        if (words.isNotEmpty()) {
            val txt = words.map { it.word }.joinToString(", ")
            Text("${relation.name}: $txt")
        }
    }
}