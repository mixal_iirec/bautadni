package com.bautadni.buffers.word

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Modifier
import com.bautadni.data.AppDatabase
import com.bautadni.data.lang.NoteData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class WordNote(data: NoteData) {
    var data by mutableStateOf(data)

    suspend fun flowPost() {
        snapshotFlow { data }.collect {
            withContext(context = Dispatchers.IO) {
                val database = AppDatabase.getInstance()
                database.note().insert(data)
            }
        }
    }

    @Composable
    fun Compose() {
        TextField(value = data.note, onValueChange = {
          data = data.copy(note = it)
        }, minLines = 2, modifier = Modifier.fillMaxWidth())
    }
}