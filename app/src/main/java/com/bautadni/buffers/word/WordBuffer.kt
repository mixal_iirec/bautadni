package com.bautadni.buffers.word

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.bautadni.R
import com.bautadni.data.AppDatabase
import com.bautadni.data.BufferData
import com.bautadni.data.lang.NoteData
import com.bautadni.data.lang.WordData
import com.bautadni.buffers.Buffer
import com.bautadni.buffers.BufferState
import com.bautadni.ui.theme.BautadniTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class WordBufferState(_data: BufferData): BufferState(_data) {
    override fun bufferConstructor() = WordBuffer()
}

class WordBuffer: Buffer() {
    private var word by mutableStateOf(WordData(0, "", ""))
    private var notes = mutableStateListOf<WordNote>()
    private var relations: Flow<List<WordRelations>>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launch(context = Dispatchers.IO) {
            val database = AppDatabase.getInstance()
            database.word().byUid(state.data.typeUid)?.let { word ->
                val note = database.note().byUid(word.uid) ?: NoteData(word.uid, "")
                withContext(context = Dispatchers.Main) {
                    this@WordBuffer.word = word
                    val note = WordNote(note)
                    notes.add(note)
                    lifecycleScope.launch { note.flowPost() }
                }
                relations = database.relation().flowAll().map {it.map {WordRelations(word.uid, it)}}
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.compose_buffer, container, false).apply {
            findViewById<ComposeView>(R.id.compose).setContent {
                BautadniTheme { Compose() }
            }
        }
    }

    @Composable
    fun Compose() {
        val scrollState = rememberScrollState()
        Column(modifier = Modifier.verticalScroll(scrollState)) {
            Row( horizontalArrangement = Arrangement.SpaceBetween, modifier = Modifier.fillMaxWidth() ) {
                // ignore warning, this crashes when a word is deleted -> null reference
                word.word.let { Text(text = it, fontSize = 48.sp) }
                word.lang.let { Text(text = it, fontSize = 48.sp) }
            }
            for (note in notes) {
                note.Compose()
            }
            relations?.let {
                val rels = it.collectAsStateWithLifecycle(initialValue = listOf()).value
                for (rel in rels) {
                    rel.Compose()
                }
            }
        }
    }
}