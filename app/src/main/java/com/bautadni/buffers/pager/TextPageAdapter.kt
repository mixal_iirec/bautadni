package com.bautadni.buffers.pager

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.BackgroundColorSpan
import android.text.style.StyleSpan
import android.view.ActionMode
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bautadni.BauWord
import com.bautadni.HighlightStyle
import com.bautadni.R
import com.bautadni.WordSelectionState
import com.bautadni.WordSelectionViewModel
import com.bautadni.WordSelectorManager

class TextPageAdapter(
    diffCallback: DiffUtil.ItemCallback<TextPage>,
    private val owner: PagerBuffer,
    private val language: String
) :
    PagingDataAdapter<TextPage, TextPageViewHolder>(diffCallback) {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TextPageViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.pdf_pager_item, parent, false)

        return TextPageViewHolder(view)
    }

    override fun onBindViewHolder(holder: TextPageViewHolder, position: Int) {
        val item = getItem(position)
        // Note that item can be null. ViewHolder must support binding a
        // null item as a placeholder.
        val viewModel: WordSelectionViewModel by owner.requireActivity().viewModels()
        holder.bind(item, owner, language, viewModel.state, viewModel.highlightedWords)
    }

    override fun onViewAttachedToWindow(holder: TextPageViewHolder) {
        super.onViewAttachedToWindow(holder)
        (holder.itemView.findViewById(R.id.pdfPagerItemText) as TextView).isEnabled = false
        (holder.itemView.findViewById(R.id.pdfPagerItemText) as TextView).isEnabled = true
    }
}

class TextPageViewHolder(
    view: View
) : ViewHolder(view) {
    fun bind(
        item: TextPage?,
        owner: PagerBuffer,
        language: String,
        selectionData: MutableLiveData<WordSelectionState>,
        highlightData: MutableLiveData<List<HighlightWord>>
    ) {
        (this.itemView.findViewById(R.id.pdfPagerItemPage) as TextView).text =
            item?.pageNumber?.let { (it + 1).toString() } ?: owner.requireContext()
                .getString(R.string.loading)

        val textTv = (this.itemView.findViewById(R.id.pdfPagerItemText) as TextView)
        textTv.text = item?.text ?: owner.requireContext().getString(R.string.loading)

        val cb = ActionModeCallback(
            textTv,
            selectionData,
            highlightData,
            owner.state.bufferring.thisRing,
            language
        )
        cb.init(owner)

        textTv.customSelectionActionModeCallback = cb
        textTv.setTextIsSelectable(true)
        val lateSelection = highlightData.value
        if (lateSelection != null) {
            cb.highlightAll(lateSelection)
        }
    }
}

object TextPageComparator : DiffUtil.ItemCallback<TextPage>() {
    override fun areItemsTheSame(oldItem: TextPage, newItem: TextPage): Boolean {
        return oldItem.pageNumber == newItem.pageNumber
    }

    override fun areContentsTheSame(oldItem: TextPage, newItem: TextPage): Boolean {
        return oldItem.text == newItem.text
    }
}

class HighlightWord(val bauWord: BauWord, val style: HighlightStyle)

class ActionModeCallback(
    private val textView: TextView,
    // only for sending selection updates
    private val selectionData: MutableLiveData<WordSelectionState>,
    // only for receiving info about what and how to highlight
    private val highlightData: MutableLiveData<List<HighlightWord>>,
    private val buffRingUid: Int,
    private val language: String
) : ActionMode.Callback {

    fun init(owner: LifecycleOwner) {
        highlightData.observe(owner) { selection ->
            resetSelection() // must always reset (for the case
            // when a word is selected and ANOTHER, different, word is selected)
            // - the old selection must disappear
            if (selection != null) {
                // if selection size == 1, fetch more words & concat with current selection
                highlightAll(selection)
            }
        }
    }

    private val selectedSpans: ArrayList<Pair<StyleSpan, BackgroundColorSpan>> = ArrayList()

    private fun resetSelection() {
        val ssb = SpannableStringBuilder(textView.text)
        for (span in selectedSpans) {
            ssb.removeSpan(span.first)
            ssb.removeSpan(span.second)
        }
        textView.setTextKeepState(ssb)
    }

    private fun highlightWord(
        start: Int,
        end: Int,
        ssb: SpannableStringBuilder,
        style: HighlightStyle
    ) {
        val st = WordSelectorManager.getStyle(style)
        val selectedSpanBold = st.styleSpanGenerator()
        val selectedSpanBgColor = st.highlightSpanGenerator()
        ssb.setSpan(selectedSpanBold, start, end, 1)
        ssb.setSpan(selectedSpanBgColor, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        selectedSpans.add(Pair(selectedSpanBold, selectedSpanBgColor))
    }

    fun highlightAll(selection: List<HighlightWord?>) {
        val targetText = textView.text
        val ssb = SpannableStringBuilder(targetText)
        for (word in selection) {
            var start = 0
            var end: Int
            if (word?.bauWord == null || word.bauWord.text.isEmpty() || word.bauWord.lang != language) {
                continue
            }
            val cleanWord = word.bauWord.text.trim()
            if (cleanWord.isEmpty()) {
                continue
            }
            while (start < targetText.length && targetText.indexOf(cleanWord, start) != -1) {
                start = targetText.indexOf(cleanWord, start)
                end = start + cleanWord.length
                highlightWord(start, end, ssb, word.style)
                start = end
            }
        }
        textView.setTextKeepState(ssb)
    }

    override fun onCreateActionMode(
        mode: ActionMode?,
        menu: Menu?
    ): Boolean {
        mode?.menu?.add(textView.context.getString(R.string.select))?.setOnMenuItemClickListener {
            val text = textView.text
            val start = textView.selectionStart
            val end = textView.selectionEnd
            val selected = text.subSequence(start, end).toString()
            if (selected.isNotEmpty()) {
                selectionData.value =
                    selectionData.value?.updatedSelectionState(selected, buffRingUid, language)
            }
            true
        }

        return true
    }

    override fun onPrepareActionMode(p0: ActionMode?, p1: Menu?): Boolean {
        return true
    }

    override fun onActionItemClicked(p0: ActionMode?, p1: MenuItem?): Boolean {
        return true
    }

    override fun onDestroyActionMode(p0: ActionMode?) {
        return
    }
}

