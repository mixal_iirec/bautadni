package com.bautadni.buffers.pager

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.flow.Flow
class TextViewModel : ViewModel() {
    lateinit var flow: Flow<PagingData<TextPage>>
    fun init(listOfPages: List<String>, initialPage: Int) {
        this.flow = Pager(
            PagingConfig(pageSize = 4, enablePlaceholders = true)
        ) {
            TextPagingSource().apply {
                init(listOfPages, initialPage)
            }
        }.flow
            .cachedIn(viewModelScope)
    }
}