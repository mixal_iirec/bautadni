package com.bautadni.buffers.pager

import androidx.paging.PagingSource
import androidx.paging.PagingState

class TextPage(var pageNumber: Int = 0,  var text: String = "")

class TextPagingSource : PagingSource<Int, TextPage>() {

    private var pages: List<String>? = null
    private var initialPage = 0

    fun init(listOfPages: List<String>, initialPage: Int) {
        this.pages = listOfPages
        this.initialPage = initialPage
    }

    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, TextPage> {
        val pageNum = params.key ?: initialPage
        val page = TextPage(pageNum, pages?.getOrNull(pageNum) ?: "")

        return LoadResult.Page(
            data = listOf(page),
            prevKey = if (pageNum > 0) pageNum - 1 else null,
            nextKey = pages?.let { if (pageNum + 1 < it.size) { pageNum + 1 } else null }
        )
    }

    override fun getRefreshKey(state: PagingState<Int, TextPage>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }
}