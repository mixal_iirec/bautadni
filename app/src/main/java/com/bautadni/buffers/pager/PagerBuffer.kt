package com.bautadni.buffers.pager

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bautadni.R
import com.bautadni.data.AppDatabase
import com.bautadni.data.BookData
import com.bautadni.data.BookType
import com.bautadni.data.BufferData
import com.bautadni.data.PagerData
import com.bautadni.buffers.Buffer
import com.bautadni.buffers.BufferState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json

class PagerBufferState(buffData: BufferData) : BufferState(buffData) {
    override fun bufferConstructor() = PagerBuffer()
}

class PagerBuffer : Buffer() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.pager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val self = this
        this.context?.let { _ ->
            lifecycleScope.launch(context = Dispatchers.IO) {
                val database = AppDatabase.getInstance()
                database.pager().byUid(self.state.data.typeUid)?.let { pager ->
                    setupPager(pager, database.book().byUid(pager.bookUid))
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        val recyclerView = requireView().findViewById<RecyclerView>(R.id.recyclerPager)
        when (recyclerView?.layoutManager) {
            is LinearLayoutManager -> {
                val pos =
                    (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                runBlocking(context = Dispatchers.IO) {
                    val database = AppDatabase.getInstance()
                    database.pager().byUid(state.data.typeUid)?.let { pager ->
                        database.pager()
                            .insert(PagerData(pager.uid, pager.bookUid, pos.toLong(), pager.lang))
                    }
                }
            }
        }
    }

    private val textViewModel: TextViewModel by viewModels()
    private suspend fun initPager3(
        pagingAdapter: PagingDataAdapter<TextPage, *>,
    ) {
        val recyclerView = requireView().findViewById<RecyclerView>(R.id.recyclerPager)
        recyclerView.adapter = pagingAdapter
        recyclerView.layoutManager =
                // https://stackoverflow.com/a/45037389
            object : LinearLayoutManager(requireContext()) {
                override fun requestChildRectangleOnScreen(
                    parent: RecyclerView,
                    child: View,
                    rect: android.graphics.Rect,
                    immediate: Boolean
                ): Boolean {
                    return false
                }

                override fun requestChildRectangleOnScreen(
                    parent: RecyclerView,
                    child: View,
                    rect: android.graphics.Rect,
                    immediate: Boolean,
                    focusedChildVisible: Boolean
                ): Boolean {
                    return false
                }
            }
        viewLifecycleOwner.lifecycleScope.launch {
            textViewModel.flow.collectLatest { pagingData ->
                withContext(context = Dispatchers.Main) {
                    pagingAdapter.submitData(pagingData)
                }
            }
        }
    }


    private suspend fun setupPager(pager: PagerData, book: BookData?) {

        withContext(context = Dispatchers.Main) {
            val pages = if (book == null) {
                listOf(getString(R.string.could_not_find_book_for_this_buffer_was_it_removed_please_close_this_buffer))
            } else when (book.type) {
                BookType.PagedPlainText.value -> {
                    try {
                        withContext(Dispatchers.IO) {
                            requireContext().openFileInput(book.path).bufferedReader()
                                .use {
                                    Json.decodeFromString<List<String>>(it.readText())
                                }.filter { it.trim().isNotEmpty() }
                        }
                    } catch (e: Exception) {
                        Log.e("PagerBuffer", "Failed to open file: $e")
                        listOf(
                            getString(
                                R.string.error_opening_file_for_reading_book,
                                book.path,
                                book.name
                            )
                        )
                    }
                }
                else -> listOf(
                    getString(
                        R.string.book_type_of_book_incompatible_with_pager_buffer,
                        book.type,
                        book.name,
                        book.path
                    )
                )
            }

            textViewModel.init(pages.ifEmpty { listOf(getString(R.string.book_is_empty_could_be_missing)) }, pager.page.toInt())
            initPager3(
                TextPageAdapter(
                    TextPageComparator,
                    this@PagerBuffer,
                    pager.lang
                )
            )
        }
    }
}
