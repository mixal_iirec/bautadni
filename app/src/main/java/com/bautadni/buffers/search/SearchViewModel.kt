package com.bautadni.buffers.search

import android.util.Log
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.bautadni.buffers.Where
import com.bautadni.data.BufferData
import com.bautadni.data.BufferType
import com.bautadni.buffers.BufferState
import com.bautadni.buffers.BufferViewModel
import com.bautadni.buffers.search.impl.BookSearch
import com.bautadni.buffers.search.impl.FormulaSearch
import com.bautadni.buffers.search.impl.RelationSearch
import com.bautadni.buffers.search.impl.WordSearch
import com.bautadni.buffers.search.impl.WorkspaceSearch
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SearchBufferState(data: BufferData): BufferState(data) {
    override fun bufferConstructor() = SearchBuffer()
}

class SearchViewModel : BufferViewModel() {
    lateinit var search: Search
    var query: SearchQuery? = null
    private var source: SearchPageSource? = null
    lateinit var flow: Flow<PagingData<SearchItem>>

    private fun reloadQuery(query: SearchQuery) {
        this.query = query
        source?.invalidate()
    }

    fun newQuery(query: SearchQuery) {
        reloadQuery(query)
        viewModelScope.launch {
            query.post()
            changeTypeUid(query.uid())
        }
    }

    fun postItem(item: SearchData, invalidate: Boolean) {
        viewModelScope.launch {
            val invalidatePost = item.post()
            if (invalidate || invalidatePost) {
                source?.invalidate()
            }
        }
    }

    fun deleteItems(items: List<SearchData>) {
        items.getOrNull(0)?.let { first ->
            viewModelScope.launch {
                first.delete(items)
                source?.invalidate()
            }
        }
    }

    fun open(state: BufferState, item: SearchData, args: SearchOpenArgs) {
        viewModelScope.launch {
            item.open(args)?.let {
                state.bufferring.open(BufferState.create(it), Where.OTHER)
            }
        }
    }

    fun init(data: BufferData) {
        search = when (data.type) {
            BufferType.SearchBook -> BookSearch()
            BufferType.SearchWorkSpace -> WorkspaceSearch()
            BufferType.SearchRelation -> RelationSearch()
            BufferType.SearchFormula -> FormulaSearch()
            BufferType.SearchWord -> WordSearch()
            else -> {
                Log.e("BUG", "Invalid search type supplied ${data.type}")
                BookSearch()
            }
        }
        flow = Pager(PagingConfig(pageSize = 16)){
            val new = search.source(query)
            source = new
            new
        }.flow.cachedIn(viewModelScope)

        viewModelScope.launch {
            withContext(context = Dispatchers.IO) {
                val query = search.query(data.typeUid)
                withContext(context = Dispatchers.Main) {
                    reloadQuery(query)
                }
            }
        }
    }
}
