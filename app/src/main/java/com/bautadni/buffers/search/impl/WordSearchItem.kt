package com.bautadni.buffers.search.impl

import android.net.Uri
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bautadni.R
import com.bautadni.buffers.search.Search
import com.bautadni.buffers.search.SearchData
import com.bautadni.buffers.search.SearchItem
import com.bautadni.buffers.search.SearchItemEdit
import com.bautadni.buffers.search.SearchOpen
import com.bautadni.buffers.search.SearchOpenArgs
import com.bautadni.buffers.search.SearchQuery
import com.bautadni.buffers.search.SearchQueryEdit
import com.bautadni.data.AppDatabase
import com.bautadni.data.BufferData
import com.bautadni.data.BufferType
import com.bautadni.data.lang.WordData
import com.bautadni.data.lang.WordQuery

class WordSearch: Search() {
    override fun name() = R.string.search_word_title
    override fun queryDefault() = WordSearchQuery(WordQuery.all())
    override suspend fun queryLoad(database: AppDatabase, queryUid: Int) = database.wordQuery().byUid(queryUid)?.let { WordSearchQuery(it) }
    override fun defaultData() = WordSearchData(WordData.default())
}

class WordSearchData(var d: WordData, private var uri: Uri? = null): SearchData() {
    override suspend fun createItem() = WordSearchItem(this)
    override fun edit() = WordSearchItemEdit(d)
    override fun openDialog() = WordSearchOpen()


    override suspend fun post(database: AppDatabase): Boolean {
        d.uid = database.word().insert(d).toInt()
        return false
    }
    override suspend fun delete(database: AppDatabase, data: List<SearchData>) = database.word().delete(data.map{(it as WordSearchData).d.uid})
    override suspend fun open(database: AppDatabase, args: SearchOpenArgs): BufferData {
        return BufferData(0, BufferType.Word, d.uid)
    }
}

class WordSearchItem(data: WordSearchData): SearchItem() {
    var data by mutableStateOf(data)

    override fun getData(): SearchData {return data}
    override fun setData(data: SearchData) {this.data = data as WordSearchData
    }

    @Composable
    override fun expand() {
        Column () {
            Text(data.d.word, fontSize = 24.sp )
            Text(data.d.lang, fontSize = 24.sp )
        }
    }
}

class WordSearchItemEdit(private val d: WordData): SearchItemEdit() {
    override fun clearUid() = run {d.uid = 0}

    var word by mutableStateOf(d.word)
    var lang by mutableStateOf(d.lang)
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(word != "")

        Column {
            TextField(
                value = word, onValueChange = {word = it},
                placeholder = @Composable {Text (stringResource(R.string.name_parenthesized), fontSize = 10.sp)},
                modifier = Modifier.padding(8.dp)
            )
            TextField(
                value = lang, onValueChange = {lang = it},
                placeholder = @Composable {Text (stringResource(R.string.language_parenthesized), fontSize = 10.sp)},
                modifier = Modifier.padding(8.dp)
            )
        }
    }
    override fun collectIn() = WordSearchData(WordData(d.uid, word, lang))
}

class WordSearchQuery(val query: WordQuery): SearchQuery() {
    override fun edit() = WordSearchQueryEdit(query)
    override suspend fun result(): ArrayList<SearchData> {
        return ArrayList(query.query().map { WordSearchData(it) })
    }
    override fun uid(): Int = query.uid
    override suspend fun post(database: AppDatabase) { query.uid = database.wordQuery().insert(query).toInt() }
}

class WordSearchQueryEdit(private val query: WordQuery): SearchQueryEdit() {
    var name by mutableStateOf(query.name)
    var lang by mutableStateOf(query.lang)
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(true)
        Column {
            TextField(
                value = name, onValueChange = { name = it },
                placeholder = @Composable { Text(stringResource(R.string.name_regex_parenthesized), fontSize = 10.sp) },
                modifier = Modifier.padding(8.dp)
            )
            TextField(
                value = lang, onValueChange = { lang = it },
                placeholder = @Composable { Text(stringResource(R.string.language_regex_parenthesized), fontSize = 10.sp) },
                modifier = Modifier.padding(8.dp)
            )
        }
    }

    override fun collect() = WordSearchQuery(WordQuery(query.uid, name, lang))
}

class WordSearchOpenArgs(): SearchOpenArgs()
class WordSearchOpen(): SearchOpen() {
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(true)

        Text(stringResource(R.string.open_qmark))
    }
    override fun collect() = WordSearchOpenArgs()
}
