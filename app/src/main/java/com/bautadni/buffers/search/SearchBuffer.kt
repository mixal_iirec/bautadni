package com.bautadni.buffers.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.windowInsetsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Card
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.fragment.app.viewModels
import androidx.paging.compose.collectAsLazyPagingItems
import com.bautadni.R
import com.bautadni.buffers.Buffer
import com.bautadni.ui.theme.BautadniTheme
import compose.icons.TablerIcons
import compose.icons.tablericons.Copy

sealed class DialogType{
    class None(): DialogType()
    class Delete(): DialogType()
    class Edit(val idx: Int?, var edit: SearchItemEdit): DialogType()
    class Search(val query: SearchQuery): DialogType()
    class Open(val data: SearchData, val args: SearchOpen): DialogType()
}

class SearchBuffer: Buffer() {
    override val viewModel: SearchViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.init(state.data)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.compose_buffer, container, false).apply {
            findViewById<ComposeView>(R.id.compose).setContent {
                BautadniTheme { SearchCompose() }
            }
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun SearchCompose() {
        val items = viewModel.flow.collectAsLazyPagingItems()

        var selectedCount by remember { mutableIntStateOf(0) }

        var dialogType by remember { mutableStateOf(DialogType.None() as DialogType) }


        val clickEdit = { clone: Boolean ->
            {
                for (i in 0..<items.itemCount) {
                    items[i]?.let {
                        if (it.selected) {
                            dialogType = if (clone) {
                                DialogType.Edit(null, it.getData().edit(true))
                            } else {
                                DialogType.Edit(i, it.getData().edit(false))
                            }
                        }
                    }
                }
            }
        }

        val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior()
        Scaffold(
            modifier = Modifier
                .nestedScroll(scrollBehavior.nestedScrollConnection)
                .windowInsetsPadding(WindowInsets(0, 0, 0, 0)),
            topBar = {
                CenterAlignedTopAppBar(
                    scrollBehavior = scrollBehavior,
                    colors = TopAppBarDefaults.topAppBarColors(
                        containerColor = MaterialTheme.colorScheme.primaryContainer,
                        titleContentColor = MaterialTheme.colorScheme.primary,
                    ),
                    title = {
                        Text(stringResource(viewModel.search.name()))
                    },
                    // How to handle collapse through insets?
                    // Beware, TopAppBar likely does not support it.
                    //windowInsets = WindowInsets(0,0,0,0)
                    windowInsets = inset,
                    actions = {
                        if (selectedCount == 1) {
                            IconButton(onClick = clickEdit(true)) {
                                Icon(imageVector = TablerIcons.Copy, contentDescription = stringResource(R.string.cpy)
                                )
                            }
                            IconButton(onClick = clickEdit(false)) {
                                Icon(imageVector = Icons.Filled.Edit, contentDescription = stringResource(R.string.edit)
                                )
                            }
                        }
                        if (selectedCount > 0) {
                            IconButton(onClick = { dialogType = DialogType.Delete() }) {
                                Icon(imageVector = Icons.Filled.Delete, contentDescription = stringResource(R.string.delete)
                                )
                            }
                        } else {
                            IconButton(onClick = { dialogType = DialogType.Edit(null, viewModel.search.defaultData().edit(false)) }) {
                                Icon(imageVector = Icons.Filled.Add, contentDescription = stringResource(R.string.add)
                                )
                            }
                        }
                    },
                    navigationIcon = {
                        if (selectedCount == 0) {
                            IconButton(onClick = { dialogType = DialogType.Search(viewModel.query ?: viewModel.search.queryDefault()) }) {
                                Icon(imageVector = Icons.Filled.Search, contentDescription = stringResource(R.string.search)
                                )
                            }
                        }
                    },
                )
            },
        ) { innerPadding ->
            LazyColumn(
                modifier = Modifier.fillMaxSize(),
                contentPadding = innerPadding,
            ) {
                val wrapper = @Composable { index: Int, item: SearchItem ->
                    val select = {
                        items[index]?.let { item ->
                            if (item.selected) {
                                item.selected = false
                                selectedCount -= 1
                            } else {
                                item.selected = true
                                selectedCount += 1
                            }
                        }
                    }
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .pointerInput(Unit) {
                                detectTapGestures(
                                    onLongPress = { select() },
                                    onTap = {
                                        if (selectedCount > 0) {
                                            select()
                                        } else {
                                            items[index]?.let { item ->
                                                val data = item.getData()
                                                data.openDialog()
                                                    ?.let { args ->
                                                        dialogType = DialogType.Open(data, args)
                                                    }
                                            }
                                        }
                                    }
                                )
                            }
                    ){
                        item.expandWrapper(index)
                    }
                }
                items(items.itemCount) { index ->
                    items[index]?.let { item -> wrapper(index, item) }
                }
            }
        }

        val dismiss = {dialogType = DialogType.None() }

        var allowed by remember {mutableStateOf(false)}
        val allow = {set: Boolean -> allowed = set}

        val confirmDialog = @Composable {confirm: () -> Unit, content: @Composable () -> Unit ->
            Dialog(onDismissRequest = dismiss) {
                Card(modifier = Modifier.fillMaxWidth(), shape = RoundedCornerShape(8.dp)) {
                    Column() {
                        Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxWidth()) {
                            content()
                        }
                        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
                            TextButton(modifier = Modifier.padding(8.dp), onClick = dismiss) { Text(stringResource(R.string.dismiss)) }
                            TextButton(modifier = Modifier.padding(8.dp), onClick = confirm, enabled = allowed) { Text(stringResource(R.string.confirm)) }
                        }
                    }
                }
            }
        }

        when (dialogType) {
            is DialogType.None -> {}
            is DialogType.Delete -> {
                allowed = true
                val confirm = {
                    selectedCount = 0
                    val toDelete = mutableListOf<SearchData>()
                    for (i in 0..<items.itemCount) {
                        items[i]?.let {
                            if (it.selected) {
                                toDelete.add(it.getData())
                            }
                        }
                    }
                    viewModel.deleteItems(toDelete)
                    dialogType = DialogType.None()
                }
                confirmDialog(confirm) {
                    Text(
                        stringResource(R.string.delete_selected_items),
                        fontSize = 20.sp,
                        modifier = Modifier.padding(4.dp, 16.dp, 4.dp, 4.dp)
                    )
                }
            }
            is DialogType.Edit -> {
                val dialog = dialogType as DialogType.Edit
                val confirm = {
                    val data = dialog.edit.collect()
                    when (dialog.idx) {
                        null -> {
                            selectedCount = 0
                            viewModel.postItem(data, true)
                        }
                        else -> {
                            items[dialog.idx]?.setData(data)
                            viewModel.postItem(data, false)
                        }
                    }
                    dialogType = DialogType.None()
                }
                confirmDialog(confirm) {
                    dialog.edit.expand(allow)
                }
            }
            is DialogType.Search -> {
                val dialog = dialogType as DialogType.Search
                val edit = dialog.query.edit()
                val confirm = {
                    viewModel.newQuery(edit.collect())
                    dialogType = DialogType.None()
                }
                confirmDialog(confirm) {
                    edit.expand(allow)
                }
            }
            is DialogType.Open -> {
                val dialog = dialogType as DialogType.Open
                val confirm = {
                    val args = dialog.args.collect()
                    viewModel.open(state, dialog.data, args)
                    dialogType = DialogType.None()
                }
                confirmDialog(confirm) {
                    dialog.args.expand(allow)
                }
            }
        }
    }
}