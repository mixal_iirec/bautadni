package com.bautadni.buffers.search.impl

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import com.bautadni.App
import com.bautadni.R
import com.bautadni.buffers.search.Search
import com.bautadni.buffers.search.SearchData
import com.bautadni.buffers.search.SearchItem
import com.bautadni.buffers.search.SearchItemEdit
import com.bautadni.buffers.search.SearchOpen
import com.bautadni.buffers.search.SearchOpenArgs
import com.bautadni.buffers.search.SearchQuery
import com.bautadni.buffers.search.SearchQueryEdit
import com.bautadni.data.AppDatabase
import com.bautadni.data.WorkspaceData
import com.bautadni.data.WorkspaceQuery
import com.bautadni.data.BufferData
import com.bautadni.dataStore
import kotlinx.coroutines.flow.map

val workspaceKey = intPreferencesKey("workspace")

class WorkspaceSearch: Search() {
    override fun name() = R.string.search_workspace_title
    override fun queryDefault() = WorkspaceSearchQuery(WorkspaceQuery.all())
    override suspend fun queryLoad(database: AppDatabase, queryUid: Int) = database.workspaceQuery().byUid(queryUid)?.let { WorkspaceSearchQuery(it) }
    override fun defaultData() = WorkspaceSearchData(WorkspaceData.default())
}

class WorkspaceSearchData(var d: WorkspaceData): SearchData() {
    override suspend fun createItem() = WorkspaceSearchItem(this)
    override fun edit() = WorkspaceSearchItemEdit(d)
    override fun openDialog() = WorkspaceSearchOpen(d.name)

    override suspend fun post(database: AppDatabase): Boolean {
        if (clone) {
            d.uid = d.cloneDtb(database)
        } else {
            d.updateWithoutBuffersOrInsert(database)
        }
        return false
    }

    override suspend fun delete(database: AppDatabase, data: List<SearchData>) {
        var currentUid = 0
        App.context.dataStore.edit { currentUid = it[workspaceKey] ?: 1 }

        database.workspace().delete(data.map{(it as WorkspaceSearchData).d.uid}.filter { it != currentUid })
    }
    override suspend fun open(database: AppDatabase, args: SearchOpenArgs): BufferData? {
        App.context.dataStore.edit { it[workspaceKey] = d.uid }
        return null
    }
}

class WorkspaceSearchItem(data: WorkspaceSearchData): SearchItem() {
    var data by mutableStateOf(data)

    override fun getData(): SearchData {return data}
    override fun setData(data: SearchData) {this.data = data as WorkspaceSearchData
    }

    @Composable
    override fun expand() {
        Column () {
            Text(data.d.name, fontSize = 24.sp )
        }
    }
    @Composable
    override fun expandWrapper(idx: Int) {
        val currentUid = App.context.dataStore.data.map {it[workspaceKey]?:1}.collectAsState(initial =0)
        var color = if (idx % 2 == 0) MaterialTheme.colorScheme.surface else MaterialTheme.colorScheme.surfaceVariant
        if (selected) {
            color = MaterialTheme.colorScheme.surfaceTint
        }

        var modifier = Modifier
            .fillMaxWidth()
            .background(color)
        if (currentUid.value == data.d.uid) {
            modifier = modifier.border(2.dp, MaterialTheme.colorScheme.error)
        }

        val self = this
        Box(modifier){
            self.expand()
        }
    }
}

class WorkspaceSearchItemEdit(private val d: WorkspaceData): SearchItemEdit() {
    override fun clearUid() = run {d.uid = 0}

    var name by mutableStateOf(d.name)
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(name != "")

        Column {
            TextField(
                value = name, onValueChange = {name = it},
                placeholder = @Composable {Text (stringResource(R.string.name_parenthesized), fontSize = 10.sp)},
                modifier = Modifier.padding(8.dp)
            )
        }
    }
    override fun collectIn() = WorkspaceSearchData(WorkspaceData(d.uid, name, d.buffers))
}

class WorkspaceSearchQuery(val query: WorkspaceQuery): SearchQuery() {
    override fun edit() = WorkspaceSearchQueryEdit(query)
    override suspend fun result(): ArrayList<SearchData> {
        return ArrayList(query.query().map { WorkspaceSearchData(it) })
    }
    override fun uid(): Int = query.uid
    override suspend fun post(database: AppDatabase) { query.uid = database.workspaceQuery().insert(query).toInt() }
}

class WorkspaceSearchQueryEdit(private val query: WorkspaceQuery): SearchQueryEdit() {
    var name by mutableStateOf(query.name)
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(true)
        TextField(
            value = name, onValueChange = {name = it},
            placeholder = @Composable {Text (stringResource(R.string.name_regex_parenthesized), fontSize = 10.sp)},
            modifier = Modifier.padding(8.dp)
        )
    }

    override fun collect() = WorkspaceSearchQuery(WorkspaceQuery(query.uid, name))
}

class WorkspaceSearchOpenArgs(): SearchOpenArgs()
class WorkspaceSearchOpen(val name: String): SearchOpen() {
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(true)
        Text(stringResource(R.string.open_name_qmark, name), modifier = Modifier.padding(16.dp))
    }
    override fun collect() = WorkspaceSearchOpenArgs()
}
