package com.bautadni.buffers.search

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.bautadni.data.AppDatabase
import com.bautadni.data.BufferData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.math.min

abstract class Search {
    abstract fun name(): Int

    abstract fun queryDefault(): SearchQuery
    protected abstract suspend fun queryLoad(database: AppDatabase, queryUid: Int): SearchQuery?
    suspend fun query(queryUid: Int): SearchQuery {
        return when (queryUid) {
            0 -> queryDefault()
            else -> queryLoad(queryUid) ?: queryDefault()
        }
    }
    fun source(query: SearchQuery?) = SearchPageSource(query)

    abstract fun defaultData(): SearchData

    suspend fun queryLoad(queryUid: Int) = withContext(context = Dispatchers.IO) {
        val database = AppDatabase.getInstance()
        return@withContext queryLoad(database, queryUid)
    }
}

abstract class SearchData {
    var clone: Boolean = false

    protected abstract fun edit(): SearchItemEdit
    fun edit(clone: Boolean): SearchItemEdit {
        val ret = edit()
        ret.clone = clone
        if (clone) {
            ret.clearUid()
        }
        return ret
    }

    abstract suspend fun createItem(): SearchItem
    protected abstract suspend fun post(database: AppDatabase): Boolean
    protected abstract suspend fun delete(database: AppDatabase, data: List<SearchData>)

    open fun openDialog(): SearchOpen? = null
    protected open suspend fun open(database: AppDatabase, args: SearchOpenArgs): BufferData? = null

    suspend fun post(): Boolean = withContext(context = Dispatchers.IO) {
        val database = AppDatabase.getInstance()
        val ret = post(database)
        clone = false
        return@withContext ret
    }
    suspend fun delete(data: List<SearchData>) = withContext(context = Dispatchers.IO) {
        val database = AppDatabase.getInstance()
        delete(database, data)
    }
    suspend fun open(args: SearchOpenArgs): BufferData? = withContext(context = Dispatchers.IO) {
        val database = AppDatabase.getInstance()
        return@withContext open(database, args)
    }
}

abstract class SearchItem {
    var selected by mutableStateOf(false)

    abstract fun getData(): SearchData
    abstract fun setData(data: SearchData)

    @Composable
    protected open fun expand() {}

    @Composable
    open fun expandWrapper(idx: Int) {
        var color = if (idx % 2 == 0) MaterialTheme.colorScheme.surface else MaterialTheme.colorScheme.surfaceVariant
        if (selected) {
            color = MaterialTheme.colorScheme.surfaceTint
        }

        val self = this
        Box(modifier = Modifier.fillMaxWidth().background(color)){
            self.expand()
        }
    }
}

abstract class SearchItemEdit {
    var clone: Boolean = false
    abstract fun clearUid()
    @Composable
    abstract fun expand(allow: (Boolean) -> Unit)
    protected abstract fun collectIn(): SearchData
    fun collect(): SearchData {
        val ret = collectIn()
        ret.clone = clone
        return ret
    }
}

abstract class SearchQuery {
    abstract fun edit(): SearchQueryEdit
    abstract suspend fun result(): ArrayList<SearchData>

    abstract fun uid(): Int
    protected abstract suspend fun post(database: AppDatabase)
    suspend fun post() = withContext(context = Dispatchers.IO) { post(AppDatabase.getInstance()) }
}

abstract class SearchQueryEdit {
    @Composable
    abstract fun expand(allow: (Boolean) -> Unit)
    abstract fun collect(): SearchQuery
}

abstract class SearchOpenArgs {}
abstract class SearchOpen {
    @Composable
    abstract fun expand(allow: (Boolean) -> Unit)
    abstract fun collect(): SearchOpenArgs
}

/* We might want to not invalidate *everything*.
 * Though so far we do not have enough data to test such implementation. */
class SearchPageSource(
    private var query: SearchQuery?,
) : PagingSource<Int, SearchItem>() {
    private var items: ArrayList<SearchData>? = null

    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, SearchItem> {
        query?.let{ query ->
            val items = this.items ?: query.result()
            this.items = items

            val start = min(params.key ?: 0, items.size)
            val end = min(start + params.loadSize, items.size)

            val data = items.slice(start..<end).map {it.createItem()}

            return LoadResult.Page(
                data = data,
                prevKey = if (start > 0) start -1 else null,
                nextKey = if (end < items.size) end else null,
            )
        }
        return LoadResult.Page(
            data = arrayListOf(),
            prevKey = null,
            nextKey = null,
        )
    }

    override fun getRefreshKey(state: PagingState<Int, SearchItem>): Int? {
        // Always reload all
        return null
    }
}