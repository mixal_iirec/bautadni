package com.bautadni.buffers.search.impl

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bautadni.App
import com.bautadni.utils.ImageToText
import com.bautadni.R
import com.bautadni.buffers.search.Search
import com.bautadni.buffers.search.SearchData
import com.bautadni.buffers.search.SearchItem
import com.bautadni.buffers.search.SearchItemEdit
import com.bautadni.buffers.search.SearchOpen
import com.bautadni.buffers.search.SearchOpenArgs
import com.bautadni.buffers.search.SearchQuery
import com.bautadni.buffers.search.SearchQueryEdit
import com.bautadni.data.AppDatabase
import com.bautadni.data.BookData
import com.bautadni.data.BookQuery
import com.bautadni.data.BookType
import com.bautadni.data.BufferData
import com.bautadni.data.BufferType
import com.bautadni.data.PagerData
import com.tom_roush.pdfbox.android.PDFBoxResourceLoader
import com.tom_roush.pdfbox.io.MemoryUsageSetting
import com.tom_roush.pdfbox.pdmodel.PDDocument
import com.tom_roush.pdfbox.text.PDFTextStripper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

class BookSearch: Search() {
    override fun name() = R.string.search_book_title
    override fun queryDefault() = BookSearchQuery(BookQuery.all())
    override suspend fun queryLoad(database: AppDatabase, queryUid: Int) = database.bookQuery().byUid(queryUid)?.let { BookSearchQuery(it) }
    override fun defaultData() = BookSearchData(BookData.default())
}

class BookSearchData(var d: BookData, private var uri: Uri? = null): SearchData() {
    override suspend fun createItem() = BookSearchItem(this)
    override fun edit() = BookSearchItemEdit(d)
    override fun openDialog() = BookSearchOpen(d.languages)

    private fun createAppPrivateFile(context: Context) : Pair<String, String> {
        val tempFile = File.createTempFile("bau", ".json", context.filesDir)
        return  Pair(context.filesDir.absolutePath, tempFile.name)
    }

    private suspend fun img(uri: Uri, context: Context) : Pair<String, String> {
        val newFile = createAppPrivateFile(context)
        ImageToText.imageToText(uri, context) { visionText ->
            runBlocking (Dispatchers.IO) {
                context.openFileOutput(newFile.second, Context.MODE_APPEND).use {
                    it.write(visionText.text.toByteArray())
                }
            }
        }
        return newFile
    }

    private suspend fun pdf(uri: Uri, context: Context) : Pair<String, String> {
        val newFile = createAppPrivateFile(context)
        context.contentResolver.openInputStream(uri)?.use { input ->
            context.openFileOutput(newFile.second, Context.MODE_APPEND).use { out ->
                PDFBoxResourceLoader.init(context)
                val mb20: Long = 20 * 1024 * 1024
                val doc = PDDocument.load(input, MemoryUsageSetting.setupMixed(mb20, mb20 * 10))
                val pageList = mutableListOf<String>()
                for (i in 0 until doc.numberOfPages) {
                    val stripper = PDFTextStripper()
                    stripper.startPage = i
                    stripper.endPage = i
                    pageList.add(stripper.getText(doc))
                }

                Json.encodeToString(pageList).let {
                    out.write(it.toByteArray())
                }
            }
        }
        return newFile
    }



    override suspend fun post(database: AppDatabase): Boolean {
        uri?.let { uri ->
            val newFile = when (d.type) {
                BookType.PDF.value -> pdf(uri, App.context)
                BookType.Image.value -> img(uri, App.context)
                else -> Pair("", "")
            }
            d = d.copy(path = newFile.second, type = BookType.PagedPlainText.value)
        }

        d.uid = database.book().insert(d).toInt()
        return uri != null
    }
    override suspend fun delete(database: AppDatabase, data: List<SearchData>) {
        data.forEach {
            val uri = (it as BookSearchData).uri
            if (uri != null) {
                App.context.contentResolver.delete(uri, null, null)
            }
        }

        database.book().delete(data.map{(it as BookSearchData).d.uid})
    }

    override suspend fun open(database: AppDatabase, args: SearchOpenArgs): BufferData {
        val args = args as BookSearchOpenArgs
        val pagerId = database.pager().insert(PagerData(0, d.uid, 0, args.lang)).toInt()
        val bufferData = BufferData(0, BufferType.Pager, pagerId)
        bufferData.uid = database.buffer().insert(bufferData).toInt()
        return bufferData
    }
}

class BookSearchItem(data: BookSearchData): SearchItem() {
    var data by mutableStateOf(data)

    override fun getData(): SearchData {return data}
    override fun setData(data: SearchData) {this.data = data as BookSearchData
    }

    @Composable
    override fun expand() {
        Column () {
            Text(data.d.name, fontSize = 24.sp )
            Text(data.d.languages.joinToString(","), fontSize = 24.sp )
        }
    }
}

class BookSearchItemEdit(private val d: BookData): SearchItemEdit() {
    override fun clearUid() = run {d.uid = 0}
    companion object {
        fun queryName(resolver: ContentResolver, uri: Uri): String =
            runBlocking (Dispatchers.IO) {
                val returnCursor =
                    resolver.query(uri, arrayOf(OpenableColumns.DISPLAY_NAME), null, null, null)
                        ?: return@runBlocking ""
                val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                returnCursor.moveToFirst()
                val name = returnCursor.getString(nameIndex)
                returnCursor.close()
                return@runBlocking name
            }
    }

    var name by mutableStateOf(d.name)
    var path by mutableStateOf(d.path)
    var type by mutableStateOf(d.type)
    var lang by mutableStateOf(d.languages.joinToString ( "," ))
    private var uri : Uri? = null
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(name != "" && path != "")

        val self = this

        val setUri = { uri: Uri?, type: String ->
            uri?.let { itUri ->
                itUri.path?.let { path ->
                    self.path = path
                    self.type = type
                    self.uri = itUri
                    if (self.name == "") {
                        self.name = queryName(App.context.contentResolver, itUri)
                    }
                }
            }
        }
        val newPdfBook = rememberLauncherForActivityResult(ActivityResultContracts.OpenDocument()) {
            setUri(it, BookType.PDF.value)
        }
        val newImageBook = rememberLauncherForActivityResult( ActivityResultContracts.PickVisualMedia() ) {
            setUri(it, BookType.Image.value)
        }

        Column {
            TextField(
                value = name, onValueChange = {name = it},
                placeholder = @Composable {Text (stringResource(R.string.name_parenthesized), fontSize = 10.sp)},
                modifier = Modifier.padding(8.dp)
            )
            TextField(
                value = lang, onValueChange = {lang = it},
                placeholder = @Composable {Text (stringResource(R.string.languages_separated_by_parenthesized), fontSize = 10.sp)},
                modifier = Modifier.padding(8.dp)
            )
            Button(onClick = {
                newPdfBook.launch(arrayOf("application/pdf"))
            }) {
                Text(text = stringResource(R.string.select_pdf))
            }
            Button(onClick = {
                newImageBook.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
            }) {
                Text(text = stringResource(R.string.select_image))
            }
        }
    }
    override fun collectIn() = BookSearchData(BookData(d.uid, name, path, type, ArrayList(lang.split(",").filter{it!=""})), uri)
}

class BookSearchQuery(val query: BookQuery): SearchQuery() {
    override fun edit() = BookSearchQueryEdit(query)
    override suspend fun result(): ArrayList<SearchData> {
        return ArrayList(query.query().map { BookSearchData(it) })
    }
    override fun uid(): Int = query.uid
    override suspend fun post(database: AppDatabase) { query.uid = database.bookQuery().insert(query).toInt() }
}

class BookSearchQueryEdit(private val query: BookQuery): SearchQueryEdit() {
    var name by mutableStateOf(query.name)
    var lang by mutableStateOf(query.lang)
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(true)
        TextField(
            value = name, onValueChange = {name = it},
            placeholder = @Composable {Text (stringResource(R.string.name_regex_parenthesized), fontSize = 10.sp)},
            modifier = Modifier.padding(8.dp)
        )
        TextField(
            value = lang, onValueChange = {lang = it},
            placeholder = @Composable {Text (stringResource(R.string.language_regex_parenthesized), fontSize = 10.sp)},
            modifier = Modifier.padding(8.dp)
        )
    }

    override fun collect() = BookSearchQuery(BookQuery(query.uid, name, lang))
}

class BookSearchOpenArgs(val lang: String): SearchOpenArgs()
class BookSearchOpen(private val langs: ArrayList<String>): SearchOpen() {
    var lang by mutableStateOf("")
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(lang != "")

        var expanded by remember {
            val startExpanded = if (langs.size == 1) {
                lang = langs[0]
                false
            } else {
                true
            }
            mutableStateOf(startExpanded)
        }
        val icon = if (expanded) Icons.Filled.KeyboardArrowUp else Icons.Filled.KeyboardArrowDown

        Column {
            Text(stringResource(R.string.open_qmark))
            TextField(
                value = lang, onValueChange = { lang = it },
                placeholder = @Composable { Text(stringResource(R.string.language_parenthesized), fontSize = 10.sp) },
                modifier = Modifier.padding(8.dp),
                trailingIcon = {
                    if (langs.size > 0) {
                        Icon(icon,
                            stringResource(R.string.languages), Modifier.clickable { expanded = !expanded })
                    }
                }
            )
            if (langs.size > 0) {
                DropdownMenu(expanded = expanded, onDismissRequest = { expanded = false }) {
                    langs.forEach { l ->
                        DropdownMenuItem(text = { Text(l) }, onClick = {lang = l; expanded = false })
                    }
                }
            }
        }
    }
    override fun collect() = BookSearchOpenArgs(lang)
}
