package com.bautadni.buffers.search.impl

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bautadni.R
import com.bautadni.buffers.search.Search
import com.bautadni.buffers.search.SearchData
import com.bautadni.buffers.search.SearchItem
import com.bautadni.buffers.search.SearchItemEdit
import com.bautadni.buffers.search.SearchOpen
import com.bautadni.buffers.search.SearchOpenArgs
import com.bautadni.buffers.search.SearchQuery
import com.bautadni.buffers.search.SearchQueryEdit
import com.bautadni.data.AppDatabase
import com.bautadni.data.BufferData
import com.bautadni.data.BufferType
import com.bautadni.data.ColorWrapper
import com.bautadni.data.lang.Formula
import com.bautadni.data.lang.FormulaData
import com.bautadni.data.lang.FormulaQuery
import com.github.skydoves.colorpicker.compose.HsvColorPicker
import com.github.skydoves.colorpicker.compose.rememberColorPickerController

class FormulaSearch: Search() {
    override fun name() = R.string.search_formula_title
    override fun queryDefault() = FormulaSearchQuery(FormulaQuery.all())
    override suspend fun queryLoad(database: AppDatabase, queryUid: Int) = database.formulaQuery().byUid(queryUid)?.let { FormulaSearchQuery(it) }
    override fun defaultData() = FormulaSearchData(FormulaData.default())
}

class FormulaSearchData(var d: FormulaData): SearchData() {
    override suspend fun createItem() = FormulaSearchItem(this)
    override fun edit() = FormulaSearchItemEdit(d)
    override fun openDialog() = FormulaSearchOpen(d.name)

    override suspend fun post(database: AppDatabase): Boolean {
        if (clone) {
            val f = Formula(d)
            f.load()
            f.clearUids()
            f.save()
            d.uid = f.data.uid
        } else {
            d.uid = database.formula().insert(d).toInt()
        }
        return false
    }

    override suspend fun delete(database: AppDatabase, data: List<SearchData>) {
        database.formula().delete(data.map{(it as FormulaSearchData).d.uid})
    }

    override suspend fun open(database: AppDatabase, args: SearchOpenArgs): BufferData? {
        return if (d.uid != 0) {
            BufferData(0, BufferType.FormulaEditor, d.uid)
        } else {
            null
        }
    }
}

class FormulaSearchItem(data: FormulaSearchData): SearchItem() {
    var data by mutableStateOf(data)

    override fun getData(): SearchData {return data}
    override fun setData(data: SearchData) {this.data = data as FormulaSearchData
    }

    @Composable
    override fun expand() {
        val circle = Modifier
            .padding(4.dp)
            .size(24.dp)
            .background(data.d.color.c, shape = CircleShape)
            .border(1.dp, Color.Black, shape = CircleShape)
        Row (modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Text(data.d.name, fontSize = 24.sp)

            Box(modifier = circle){}
        }
    }
}

class FormulaSearchItemEdit(private val d: FormulaData): SearchItemEdit() {
    override fun clearUid() = run {d.uid = 0}

    var name by mutableStateOf(d.name)
    var color by mutableStateOf(d.color.c)
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(name != "")

        var picker by remember { mutableStateOf(false) }

        val circle = Modifier.padding(4.dp).size(48.dp)
            .background(color, shape = CircleShape)
            .border(1.dp, if (picker) Color.DarkGray else Color.Black, shape = CircleShape)

        Column (modifier = Modifier.fillMaxWidth()) {
            TextField(
                value = name, onValueChange = {name = it},
                placeholder = @Composable {Text (stringResource(R.string.name_parenthesized), fontSize = 10.sp)},
                modifier = Modifier.padding(8.dp)
            )

            Box {
                IconButton(modifier = circle, onClick = { picker = !picker }) { }

                if (picker) {
                    val controller = rememberColorPickerController()
                    HsvColorPicker(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(300.dp)
                            .padding(10.dp),
                        controller = controller,
                        onColorChanged = { color = it.color }
                    )
                }
            }
        }
    }
    override fun collectIn() = FormulaSearchData(FormulaData(d.uid, d.innerUid, name, ColorWrapper(color)))
}

class FormulaSearchQuery(val query: FormulaQuery): SearchQuery() {
    override fun edit() = FormulaSearchQueryEdit(query)
    override suspend fun result(): ArrayList<SearchData> {
        return ArrayList(query.query().map { FormulaSearchData(it) })
    }
    override fun uid(): Int = query.uid
    override suspend fun post(database: AppDatabase) { query.uid = database.formulaQuery().insert(query).toInt() }
}

class FormulaSearchQueryEdit(private val query: FormulaQuery): SearchQueryEdit() {
    var name by mutableStateOf(query.name)
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(true)
        TextField(
            value = name, onValueChange = {name = it},
            placeholder = @Composable {Text (stringResource(R.string.name_regex_parenthesized), fontSize = 10.sp)},
            modifier = Modifier.padding(8.dp)
        )
    }

    override fun collect() = FormulaSearchQuery(FormulaQuery(query.uid, name))
}

class FormulaSearchOpenArgs(): SearchOpenArgs()
class FormulaSearchOpen(val name: String): SearchOpen() {
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(true)
        Text(stringResource(R.string.open_name_qmark, name), modifier = Modifier.padding(16.dp))
    }
    override fun collect() = FormulaSearchOpenArgs()
}
