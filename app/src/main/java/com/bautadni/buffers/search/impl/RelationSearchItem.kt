package com.bautadni.buffers.search.impl

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.IconButton
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bautadni.R
import com.bautadni.buffers.search.Search
import com.bautadni.buffers.search.SearchData
import com.bautadni.buffers.search.SearchItem
import com.bautadni.buffers.search.SearchItemEdit
import com.bautadni.buffers.search.SearchOpenArgs
import com.bautadni.buffers.search.SearchQuery
import com.bautadni.buffers.search.SearchQueryEdit
import com.bautadni.data.AppDatabase
import com.bautadni.data.ColorWrapper
import com.bautadni.data.lang.RelationData
import com.bautadni.data.lang.RelationQuery
import com.github.skydoves.colorpicker.compose.HsvColorPicker
import com.github.skydoves.colorpicker.compose.rememberColorPickerController

class RelationSearch: Search() {
    override fun name() = R.string.search_relation_title
    override fun queryDefault() = RelationSearchQuery(RelationQuery.all())
    override suspend fun queryLoad(database: AppDatabase, queryUid: Int) = database.relationQuery().byUid(queryUid)?.let { RelationSearchQuery(it) }
    override fun defaultData() = RelationSearchData(RelationData.default())
}

class RelationSearchData(var d: RelationData): SearchData() {
    override suspend fun createItem() = RelationSearchItem(this)
    override fun edit() = RelationSearchItemEdit(d)
    override fun openDialog() = null

    override suspend fun post(database: AppDatabase): Boolean {
        d.uid = database.relation().insert(d).toInt()
        return false
    }

    override suspend fun delete(database: AppDatabase, data: List<SearchData>) {
        database.relation().delete(data.map{(it as RelationSearchData).d.uid})
    }
    override suspend fun open(database: AppDatabase, args: SearchOpenArgs) = null
}

class RelationSearchItem(data: RelationSearchData): SearchItem() {
    var data by mutableStateOf(data)

    override fun getData(): SearchData {return data}
    override fun setData(data: SearchData) {this.data = data as RelationSearchData
    }

    @Composable
    override fun expand() {
        val circle = Modifier
            .padding(4.dp)
            .size(24.dp)
            .background(data.d.color.c, shape = CircleShape)
            .border(1.dp, Color.Black, shape = CircleShape)
        Row (modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Text(data.d.name, fontSize = 24.sp)

            Box(modifier = circle){}
        }
    }
}

class RelationSearchItemEdit(private val d: RelationData): SearchItemEdit() {
    override fun clearUid() = run {d.uid = 0}

    var name by mutableStateOf(d.name)
    var color by mutableStateOf(d.color.c)
    var transitive by mutableStateOf(d.transitive)
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(name != "")

        var picker by remember { mutableStateOf(false) }

        val circle = Modifier
            .padding(4.dp)
            .size(48.dp)
            .background(color, shape = CircleShape)
            .border(1.dp, if (picker) Color.DarkGray else Color.Black, shape = CircleShape)

        Column (modifier = Modifier.fillMaxWidth()) {
            TextField(
                value = name, onValueChange = { name = it },
                placeholder = @Composable { Text( stringResource(R.string.name_parenthesized), fontSize = 10.sp ) },
                modifier = Modifier.padding(8.dp)
            )

            Row (Modifier.height(24.dp)) {
                Text(stringResource(R.string.transitive), textAlign = TextAlign.Center)
                Switch(checked = transitive, onCheckedChange = { transitive = it })
            }

            Box {
                IconButton(modifier = circle, onClick = { picker = !picker }) { }

                if (picker) {
                    val controller = rememberColorPickerController()
                    HsvColorPicker(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(300.dp)
                            .padding(10.dp),
                        controller = controller,
                        onColorChanged = { color = it.color }
                    )
                }
            }
        }
    }
    override fun collectIn() = RelationSearchData(RelationData(d.uid, name, transitive, ColorWrapper(color)))
}

class RelationSearchQuery(val query: RelationQuery): SearchQuery() {
    override fun edit() = RelationSearchQueryEdit(query)
    override suspend fun result(): ArrayList<SearchData> {
        return ArrayList(query.query().map { RelationSearchData(it) })
    }
    override fun uid(): Int = query.uid
    override suspend fun post(database: AppDatabase) { query.uid = database.relationQuery().insert(query).toInt() }
}

class RelationSearchQueryEdit(private val query: RelationQuery): SearchQueryEdit() {
    var name by mutableStateOf(query.name)
    @Composable
    override fun expand(allow: (Boolean) -> Unit) {
        allow(true)
        TextField(
            value = name, onValueChange = {name = it},
            placeholder = @Composable {Text (stringResource(R.string.name_regex_parenthesized), fontSize = 10.sp)},
            modifier = Modifier.padding(8.dp)
        )
    }

    override fun collect() = RelationSearchQuery(RelationQuery(query.uid, name))
}