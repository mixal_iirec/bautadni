package com.bautadni.buffers

import android.annotation.SuppressLint
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.SCROLL_STATE_IDLE
import com.bautadni.BufferActivityUp
import com.bautadni.data.BufferData

@SuppressLint("NotifyDataSetChanged") /* We are reindexing everything. */
class Bufferring(
    override val activity: BufferActivityUp,
    override val thisRing: Int,
    override val otherRing: Int,
    buffers: ArrayList<BufferData>,
): ViewPager2.OnPageChangeCallback(), BufferringUp {

    private var nextUid: Long = 0
    var buffers = buffers.map { val buffer = BufferState.create(it); buffer.bufferring = this; Pair(buffer, nextUid++) } as ArrayList<Pair<BufferState, Long>>
        private set

    private var bufferCount by mutableIntStateOf(buffers.size)
    override fun bufferCount() = snapshotFlow { bufferCount }

    val count get() = buffers.size
    private var position = 0

    /* Should be nonnull in any ordinary circumstances. */
    var pager: ViewPager2? = null

    fun buffer(absPosition: Int): Pair<BufferState, Long> = buffers[absPosition % count]

    private fun getAbsolutePosition(position: Int): Int {
        return when (count) {
            0 -> 16
            else -> Integer.MAX_VALUE / 2 - (Integer.MAX_VALUE / 2) % count + position
        }
    }
    fun setupViewPager(activity: FragmentActivity, pager: ViewPager2) {
        this.pager = pager

        if (buffers.size <= 1) {
            pager.isUserInputEnabled = false
        }
        pager.adapter = BufferringAdapter(activity, this)
        pager.setCurrentItem(this.getAbsolutePosition(position), false)
        pager.registerOnPageChangeCallback(this)
    }

    fun unsetViewPager() { pager = null }

    private fun switchToNewBuffer(new: BufferState) {
        new.bufferring = this
        val pos = position
        val temporary = new.temporary
        new.temporary = false

        buffers.add(pos, Pair(new, nextUid++))
        bufferCount = buffers.size
        pager?.let {
            it.adapter?.notifyDataSetChanged()
            it.isUserInputEnabled = true
            it.setCurrentItem(this.getAbsolutePosition(pos) + 1, false)
            it.setCurrentItem(this.getAbsolutePosition(pos), true)
        }
        activity.saveWorkspace()
        new.temporary = temporary
    }

    fun removeBuffer(idx: Int, fast: Boolean = false): Boolean {
        if (count == 1) {
            return false
        }
        /* First scroll away. */
        if (position == idx && !fast) {
            buffer(position).first.temporary = true
            pager?.let { pager ->
                val absPos = pager.currentItem
                pager.setCurrentItem(absPos + 1, true)
            }

            return false
        }

        buffers.removeAt(idx)
        bufferCount = buffers.size
        if (position > idx) {
            position -= 1
        }

        pager?.let {
            it.adapter?.notifyDataSetChanged()
            it.setCurrentItem(this.getAbsolutePosition(position), false)
            if (count == 1) {
                it.isUserInputEnabled = false
            }
        }
        activity.saveWorkspace()
        return true
    }

    var lastIdx: Int? = 0
    override fun onPageSelected(position: Int) {
        super.onPageSelected(position)
        lastIdx = if (buffer(this.position).first.temporary) {
            this.position
        } else {
            null
        }
        this.position = position % count
    }

    override fun onPageScrollStateChanged(state: Int) {
        super.onPageScrollStateChanged(state)
        if (state == SCROLL_STATE_IDLE) {
            lastIdx?.let {
                pager?.handler?.post { removeBuffer(it) }
                lastIdx = null
            }
        }
    }

    /* Up Follows. */


    override fun open(state: BufferState, where: Where) {
        when (where) {
            Where.HERE -> switchToNewBuffer(state)
            Where.OTHER -> activity.open(state, otherRing)
        }
    }

    override fun move() {
        val state = buffer(position).first
        if (removeBuffer(position, true)) {
            activity.open(state, otherRing)
        }
    }

    override fun close() {
        removeBuffer(position)
    }
}
class BufferringAdapter(fragmentActivity: FragmentActivity, private val br: Bufferring) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount() = when(br.count) {
        0 -> 0
        else -> Int.MAX_VALUE
    }
    override fun createFragment(position: Int) = br.buffer(position).first.construct()
    override fun getItemId(position: Int) = br.buffer(position).second
    override fun containsItem(itemId: Long): Boolean = br.buffers.any {it.second == itemId}
}