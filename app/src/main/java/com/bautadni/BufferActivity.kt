package com.bautadni

import android.os.Bundle
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.widget.ViewPager2
import com.bautadni.data.AppDatabase
import com.bautadni.data.BufferData
import com.bautadni.data.BufferType
import com.bautadni.data.lang.Formula
import com.bautadni.data.lang.WordRelation
import com.bautadni.buffers.pager.HighlightWord
import com.bautadni.buffers.BufferState
import com.bautadni.ui.theme.BautadniTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.roundToInt


class BufferActivity : FragmentActivity(R.layout.two_bar) {
    val overlay get() = viewModel.overlay

    private val selectorMgr = WordSelectorManager(this)

    private fun resetSelectionState(buffRingId: Int) {
        viewModel.selectionMode = SelectionMode.None
        selectorMgr.resetSelection(buffRingId)
        viewModel.currentWords = wordSelectionViewModel.getSelectedWords()
    }

    val workspaceKey = intPreferencesKey("workspace")
    private val viewModel: BufferActivityViewModel by viewModels()
    private val wordSelectionViewModel: WordSelectionViewModel by viewModels()

    private lateinit var snackbarHostState: SnackbarHostState

    override fun onCreate(savedInstanceState: Bundle?) {
        if (viewModel.fullscreen) {
            enableEdgeToEdge()
        }

        super.onCreate(savedInstanceState)

        snackbarHostState = SnackbarHostState()
        findViewById<ComposeView>(R.id.compose_snack).setContent {
            BautadniTheme {
                Column {
                    Box(modifier = Modifier.fillMaxHeight(0.8f)) {}
                    SnackbarHost(hostState = snackbarHostState)
                }
            }
        }


        val self = this
        lifecycleScope.launch(context = Dispatchers.Main) {
            setupWorkspace()

            onBackPressedDispatcher.addCallback(self) { viewModel.handleBack() }

            viewModel.state.workspace.observe(self) { workspace ->
                lifecycleScope.launch {
                    viewModel.saveWorkspace(workspace)
                }
            }

            if (savedInstanceState == null) {
                viewModel.onceSetupFlows(wordSelectionViewModel)
            }

            lifecycleScope.launch(context = Dispatchers.Main) {
                applicationContext.dataStore.data.map { it[workspaceKey] ?: 1 }.collect { uid ->
                    if (uid != viewModel.state.workspace.value?.uid) {
                        /* Destroy everything and recreate. We do not want most of it, and it is a
                         * needless headache to handle those few things which we (for now) would want
                         * to keep. */
                        viewModel.state.bufferrings.forEach {
                            selectorMgr.resetSelection(it.thisRing)
                        }
                        viewModel.currentWords = listOf()
                        val g = findViewById<ViewGroup>(R.id.layout_bar)
                        g.removeAllViews()
                        g.refreshDrawableState()
                        recreate()
                    }
                }
            }

            selectorMgr.getSelectionLiveData().observe(self) { selection ->
                toggleButtonBar(selection)
            }

            findViewById<ComposeView>(R.id.compose_button_bar).setContent {
                BautadniTheme { ComposeButtons() }
            }
        }
    }

    private val containers = arrayOf(R.id.container1, R.id.container2)
    private suspend fun setupWorkspace() = withContext(context = Dispatchers.Main) {
        var uid = 0
        App.context.dataStore.edit { uid = it[workspaceKey] ?: 1 }
        viewModel.setupWorkspace(uid)
        containers.zip(viewModel.state.bufferrings).forEach { (container, bufferring) ->
            bufferring.setupViewPager(this@BufferActivity, findViewById(container))
            findViewById<ViewPager2>(container).registerOnPageChangeCallback(object :
                ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    if (viewModel.lastPosition[bufferring.thisRing] == position) {
                        viewModel.lastPosition[bufferring.thisRing] = position
                        return
                    }
                    viewModel.lastPosition[bufferring.thisRing] = position
                    resetSelectionState(bufferring.thisRing)
                    wordSelectionViewModel.relatedHighlightLock.value = ""
                }
            })
        }
    }

    fun toPxl(dp: Float): Int = (dp * resources.displayMetrics.density).roundToInt()

    @Composable
    private fun ComposeButtons() {
        if (viewModel.selectionMode == SelectionMode.None) return

        val scrollState = rememberScrollState()
        val pad = 2.dp
        Row(modifier = Modifier
            .height(48.dp)
            .horizontalScroll(scrollState)
            .background(MaterialTheme.colorScheme.primaryContainer)
        ) {
            when (viewModel.selectionMode) {
                SelectionMode.None -> {}
                SelectionMode.OneWord -> {
                    Button(modifier = Modifier.padding(pad, 0.dp), onClick = {
                        lifecycleScope.launch {
                            val database = AppDatabase.getInstance()
                            val list = wordSelectionViewModel.getSelectedWords()
                            if (list.size == 1) {
                                val word = database.word()
                                    .byWordLang(list[0].first.word, list[0].first.lang)
                                viewModel.state.open(
                                    BufferState.create(
                                        BufferData(
                                            0,
                                            BufferType.Word,
                                            word.uid
                                        )
                                    ).also{it.temporary = true}, (list[0].second + 1) % 2
                                )
                            }
                        }
                    }) { Text(stringResource(R.string.notes)) }

                    val formulasCollected =
                        viewModel.formulasFlow.collectAsStateWithLifecycle(initialValue = listOf())
                    val relations =
                        viewModel.relationsFlow.collectAsStateWithLifecycle(initialValue = listOf())
                    val formulas =
                        formulasCollected.value + relations.value.map { Formula.simpleRelation(it) }
                    for (form in formulas) {
                        Button(
                            onClick = {
                                viewModel.currentWords = wordSelectionViewModel.getSelectedWords()
                                viewModel.currentFormula = form
                            },
                            colors = ButtonDefaults.buttonColors(form.data.color.c),
                            modifier = Modifier.padding(pad, 0.dp)
                        )
                        { Text(form.data.name) }
                    }
                }

                SelectionMode.TwoWords -> {
                    val textAddRel = stringResource(R.string.added_relation)
                    val textRemRel = stringResource(R.string.removed_relation)

                    val relations =
                        viewModel.relationsFlow.collectAsStateWithLifecycle(initialValue = listOf())
                    for (rel in relations.value) {
                        Button(
                            onClick = {
                                lifecycleScope.launch {
                                    val list = wordSelectionViewModel.getSelectedWords()
                                    if (list.size == 2) {
                                        val added =
                                            WordRelation.toggle(
                                                rel.uid,
                                                list[0].first,
                                                list[1].first
                                            )
                                        snackbarHostState.showSnackbar(if (added) textAddRel else textRemRel)
                                    }
                                }
                            },
                            colors = ButtonDefaults.buttonColors(rel.color.c),
                            modifier = Modifier.padding(pad, 0.dp)
                        ) { Text(rel.name) }
                    }
                }
            }
        }
    }

    private fun highlightSelected(selection: WordSelectionState?): List<HighlightWord> {
        if (selection == null) {
            return listOf()
        }
        val clearSelection = selection.selection.selected.filterNotNull()
        return clearSelection.map {
            HighlightWord(
                it,
                WordSelectorManager.defaultStyle
            )
        }
    }

    private fun toggleButtonBar(selection: WordSelectionState?) {
        val pack = highlightSelected(selection)
        wordSelectionViewModel.highlightedWords.value = pack
        val newSelectionMode = when (pack.size) {
            0 -> SelectionMode.None
            1 -> SelectionMode.OneWord
            else -> SelectionMode.TwoWords
        }

        viewModel.selectionMode = newSelectionMode
    }

    override fun onDestroy() {
        viewModel.state.let { it.bufferrings.forEach { b -> b.unsetViewPager() } }
        super.onDestroy()
    }
}