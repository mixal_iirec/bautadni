package com.bautadni

import com.bautadni.buffers.BufferState

interface BufferActivityUp {
    fun open(state: BufferState, at: Int)
    fun saveWorkspace()
}