package com.bautadni.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

val OurBlue = Color(0xFFaebbff)

val VeryLightGray1 = Color(0xFFf7f7ff)
val VeryLightGray2 = Color(0xFFefefff)
val TintedLightGray = Color(0xFFbfbfff)

val TransparentLight = Color(0x10808080)
