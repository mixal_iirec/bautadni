package com.bautadni.utils

import android.content.Context
import android.net.Uri
import android.util.Log
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.Text
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions

class ImageToText {
    companion object {
        private val textRecognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)
        fun imageToText(uri: Uri, context: Context, callback: (Text) -> Unit){
            try {
                val image = InputImage.fromFilePath(context, uri)
                textRecognizer.process(image)
                    .addOnSuccessListener { visionText ->
                        callback(visionText)
                    }
                    .addOnFailureListener { e ->
                        Log.e("MLKit", "Error: $e")
                    }
            } catch (e: Exception) {
                Log.e("MLKit", "Error processing image", e)
            }
        }
    }
}