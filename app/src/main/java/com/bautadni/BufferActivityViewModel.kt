package com.bautadni

import android.graphics.Typeface
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bautadni.data.AppDatabase
import com.bautadni.data.BufferData
import com.bautadni.data.BufferType
import com.bautadni.data.WorkspaceData
import com.bautadni.data.lang.Formula
import com.bautadni.data.lang.RelationData
import com.bautadni.data.lang.WordData
import com.bautadni.buffers.pager.HighlightWord
import com.bautadni.buffers.BufferState
import com.bautadni.buffers.Bufferring
import com.bautadni.buffers.Where
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class BufferActivityState(): BufferActivityUp {
    var workspace = MutableLiveData<WorkspaceData>()

    lateinit var bufferrings: ArrayList<Bufferring>

    override fun open(state: BufferState, at: Int) {
        bufferrings[at].open(state, Where.HERE)
    }

    override fun saveWorkspace() {
        workspace.value?.let {workspaceData ->
            val buffers = bufferrings.map {ring: Bufferring -> ring.buffers.map {it.first.data}.filter{
                    it.uid != 0 && it.type != BufferType.SearchWorkSpace
                }.map {it.uid}
            } as ArrayList<ArrayList<Int>>
            if (workspaceData.buffers != buffers) {
                workspace.value = WorkspaceData(workspaceData.uid, workspaceData.name, buffers)
            }
        }
    }
}

enum class SelectionMode {
    OneWord, TwoWords, None
}

class BufferActivityViewModel: ViewModel() {
    val state = BufferActivityState()
    var fullscreen = true
    var overlay by mutableStateOf(false)
    var selectionMode: SelectionMode by mutableStateOf(SelectionMode.None)
    val lastPosition: Array<Int?> = arrayOf(null, null)

    lateinit var relationsFlow: Flow<List<RelationData>>
    lateinit var formulasFlow: Flow<List<Formula>>
    lateinit var highlightFlow: Flow<List<HighlightWord>>

    var currentWords by mutableStateOf(listOf<Pair<WordData, Int>>())
    var currentFormula by mutableStateOf(Formula.default())

    fun onceSetupFlows(wordSelectionViewModel: WordSelectionViewModel) {
        val database = AppDatabase.getInstance()
        relationsFlow = database.relation().flowAll()
        formulasFlow = database.formula().flowAll().map { it.map {
            val formula = Formula(it)
            formula.load()
            formula
        }}
        highlightFlow = snapshotFlow { Pair(currentWords, currentFormula) }.map { (ws, f) ->
            when (ws.size) {
                1 -> withContext(context = Dispatchers.IO) {
                    val w = ws[0].first
                    val res = f.apply(w.word, w.lang)
                    res.map {
                        HighlightWord(BauWord(it.word, it.lang, 0), HighlightStyle(
                            Typeface.BOLD, f.data.color.toAndroidColor()))
                    }
                }
                else -> listOf()
            }
        }

        /* This one might be time consuming with large word database. */
        viewModelScope.launch { highlightFlow.collect {
            wordSelectionViewModel.unhighlightAll()
            wordSelectionViewModel.addHighlight(it)
        } }
    }

    suspend fun setupWorkspace(uid: Int) = withContext(context = Dispatchers.IO){
        state.workspace.value?.let { workspace ->
            if (workspace.uid == uid) {
                return@withContext
            }
        }
        lastPosition.fill(null)

        val database = AppDatabase.getInstance()
        val workspace = database.workspace().byUid(uid)
            ?: database.workspace().any()
            ?: run {
                val workspace = WorkspaceData(1, "empty", arrayListOf(arrayListOf(), arrayListOf()))
                database.workspace().insert(workspace)
                workspace
            }

        if (workspace.buffers.isEmpty()) {
            workspace.buffers.add(arrayListOf())
            workspace.buffers.add(arrayListOf())
        }

        for (bufferring in workspace.buffers) {
            if (bufferring.isEmpty()) {
                /* Invalid ID, just so the bufferring is not empty. */
                bufferring.add(0)
            }
        }

        val asyncBuffers = workspace.buffers.map { it.map { uid -> async {BufferData.unnull(database.buffer().byUid(uid))} } } as ArrayList<ArrayList<Deferred<BufferData>>>
        withContext(context = Dispatchers.Main) {
            state.bufferrings = asyncBuffers.mapIndexed {idx, it -> Bufferring(state, idx, (idx + 1) % 2, it.map { it.await() } as ArrayList<BufferData>) } as ArrayList<Bufferring>
            state.workspace.value = workspace
        }
    }

    suspend fun saveWorkspace(workspace: WorkspaceData) = withContext(context = Dispatchers.IO) {
        val database = AppDatabase.getInstance()
        database.workspace().update(workspace.uid, workspace.buffers)
    }

    fun handleBack() {
        overlay = !overlay
    }
}