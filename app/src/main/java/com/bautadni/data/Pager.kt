package com.bautadni.data

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query

@Entity
data class PagerData (
    @PrimaryKey(autoGenerate = true) var uid: Int = 0,
    @ColumnInfo(name = "book") val bookUid: Int,
    @ColumnInfo(name = "page") val page: Long,
    @ColumnInfo(name = "lang") val lang: String,
) {
    suspend fun cloneDtb(database: AppDatabase): Int {
        uid = 0
        return database.pager().insert(this).toInt()
    }
}

@Dao
interface PagerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(pager: PagerData): Long
    @Insert
    suspend fun insert(vararg pager: PagerData)

    @Query("select * from PagerData where uid=:uid ")
    fun byUid(uid: Int): PagerData?
}
