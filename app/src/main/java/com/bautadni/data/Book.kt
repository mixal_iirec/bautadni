package com.bautadni.data

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query

enum class BookType(val value: String) {
    PagedPlainText("paged_text"),
    PDF("pdf"),
    Image("img"),
}

@Entity
data class BookData (
    @PrimaryKey(autoGenerate = true) var uid: Int = 0,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "path") val path: String,
    @ColumnInfo(name = "type") val type: String?,
    @ColumnInfo(name = "languages") val languages: ArrayList<String>,
) {
    companion object {
        fun default() = BookData(0, "", "", null, arrayListOf())
    }
}

@Dao
interface BookDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(book: BookData): Long

    @Query("delete from BookData where uid in (:uids)")
    suspend fun delete(uids: List<Int>)


    @Query("select * from BookData where uid=:uid ")
    suspend fun byUid(uid: Int): BookData?

    @Query("select * from BookData order by name")
    suspend fun all(): List<BookData>

    @Query("select count(*) from WorkspaceData")
    fun size(): Int
}

@Entity
class BookQuery(
    @PrimaryKey(autoGenerate = true) var uid: Int = 0,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "lang") val lang: String,
) {
    companion object {
        fun all() = BookQuery (0, "", "")
    }

    fun match(book: BookData): Boolean {
        if (name != "" && !name.toRegex().containsMatchIn(book.name)) {
            return false
        }
        if (lang != "") {
            val regex = lang.toRegex()
            if (!book.languages.any {regex.containsMatchIn(it)}) {
                return false
            }
        }
        return true
    }

    suspend fun query(): List<BookData> {
        val database = AppDatabase.getInstance()
        return database.book().all().filter {match(it)}
    }

    suspend fun cloneDtb(database: AppDatabase): Int {
        uid = 0
        return database.bookQuery().insert(this).toInt()
    }
}

@Dao
interface BookQueryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(query: BookQuery): Long

    @Query("select * from BookQuery where uid=:uid ")
    suspend fun byUid(uid: Int): BookQuery?
}
