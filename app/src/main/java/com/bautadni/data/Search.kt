package com.bautadni.data

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query

@Entity
data class SearchData (
    @PrimaryKey(autoGenerate = true) var uid: Int = 0,
    @ColumnInfo(name = "query") var queryUid: Int,
) {
    suspend fun cloneDtb(database: AppDatabase, type: BufferType): Int {
        queryUid = when (type) {
            BufferType.Pager,
            BufferType.FormulaEditor,
            BufferType.Word,
            BufferType.Settings,
            BufferType.Invalid,
            -> 0

            BufferType.SearchBook -> database.bookQuery().byUid(queryUid)?.cloneDtb(database) ?: 0
            BufferType.SearchWord -> database.wordQuery().byUid(queryUid)?.cloneDtb(database) ?: 0
            BufferType.SearchRelation -> database.relationQuery().byUid(queryUid)?.cloneDtb(database) ?: 0
            BufferType.SearchFormula -> database.formulaQuery().byUid(queryUid)?.cloneDtb(database) ?: 0
            BufferType.SearchWorkSpace -> database.workspaceQuery().byUid(queryUid)?.cloneDtb(database) ?: 0
        }
        return database.search().insert(this).toInt()
    }
}

@Dao
interface SearchDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(search: SearchData): Long

    @Query("select * from SearchData where uid=:uid ")
    fun byUid(uid: Int): SearchData?
}
