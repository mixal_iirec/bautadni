package com.bautadni.data

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query


@Entity
class WorkspaceData (
    @PrimaryKey(autoGenerate = true) var uid: Int = 0,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "buffers") val buffers: ArrayList<ArrayList<Int>>,
) {
    companion object {
        fun default() = WorkspaceData(0, "", arrayListOf())
    }

    suspend fun cloneDtb(database: AppDatabase): Int {
        uid = 0
        buffers.forEach {
            for (i in 0..<it.size) {
                it[i] = database.buffer().byUid(it[i])?.cloneDtb(database) ?: 0
            }
        }
        return database.workspace().insert(this).toInt()
    }

    suspend fun updateWithoutBuffersOrInsert(database: AppDatabase) {
        if (database.workspace().insertIgnore(this).toInt() == -1) {
            database.workspace().update(this.uid, this.name)
        }
    }
}

@Dao
interface WorkspaceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(workspace: WorkspaceData): Long
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertIgnore(workspace: WorkspaceData): Long

    @Query("update WorkspaceData set name=:name where uid=:uid")
    suspend fun update(uid: Int, name: String)

    @Query("update WorkspaceData set buffers=:buffers where uid=:uid")
    suspend fun update(uid: Int, buffers: ArrayList<ArrayList<Int>>)

    @Query("delete from WorkspaceData where uid in (:uids)")
    suspend fun delete(uids: List<Int>)

    @Query("select * from WorkspaceData where uid=:uid")
    suspend fun byUid(uid: Int): WorkspaceData?

    @Query("select * from WorkspaceData limit 1")
    suspend fun any(): WorkspaceData?

    @Query("select * from WorkspaceData order by name")
    suspend fun all(): List<WorkspaceData>

    @Query("select count(*) from WorkspaceData")
    suspend fun size(): Int
}

@Entity
class WorkspaceQuery(
    @PrimaryKey(autoGenerate = true) var uid: Int = 0,
    @ColumnInfo(name = "name") val name: String,
) {
    companion object {
        fun all() = WorkspaceQuery (0, "")
    }

    fun match(workspace: WorkspaceData): Boolean {
        if (name != "" && !name.toRegex().containsMatchIn(workspace.name)) {
            return false
        }
        return true
    }

    suspend fun query(): List<WorkspaceData> {
        val database = AppDatabase.getInstance()
        return database.workspace().all().filter {match(it)}
    }

    suspend fun cloneDtb(database: AppDatabase): Int {
        uid = 0
        return database.workspaceQuery().insert(this).toInt()
    }
}

@Dao
interface WorkspaceQueryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(query: WorkspaceQuery): Long

    @Query("select * from WorkspaceQuery where uid=:uid ")
    suspend fun byUid(uid: Int): WorkspaceQuery?
}
