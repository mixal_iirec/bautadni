package com.bautadni.data

import androidx.compose.ui.graphics.Color
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.bautadni.App
import com.bautadni.data.lang.FormulaDao
import com.bautadni.data.lang.FormulaData
import com.bautadni.data.lang.FormulaInnerData
import com.bautadni.data.lang.FormulaQuery
import com.bautadni.data.lang.FormulaQueryDao
import com.bautadni.data.lang.LanguageDao
import com.bautadni.data.lang.LanguageData
import com.bautadni.data.lang.NoteDao
import com.bautadni.data.lang.NoteData
import com.bautadni.data.lang.RelationDao
import com.bautadni.data.lang.RelationData
import com.bautadni.data.lang.RelationQuery
import com.bautadni.data.lang.RelationQueryDao
import com.bautadni.data.lang.WordDao
import com.bautadni.data.lang.WordData
import com.bautadni.data.lang.WordQuery
import com.bautadni.data.lang.WordQueryDao
import com.bautadni.data.lang.WordRelation
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

/* Because Room refuses to type convert Color on its own. */
data class ColorWrapper(val c: Color) {
    fun toAndroidColor(): Int = android.graphics.Color.argb(
        c.alpha,
        c.red,
        c.green,
        c.blue
    )
}

class Converters {
    @TypeConverter
    fun fromArray(value : ArrayList<ArrayList<Int>>) = Json.encodeToString(value)
    @TypeConverter
    fun toArray(value: String) = Json.decodeFromString<ArrayList<ArrayList<Int>>>(value)

    @TypeConverter
    fun fromStringArray(value : ArrayList<String>) = Json.encodeToString(value)
    @TypeConverter
    fun toStringArray(value: String) = Json.decodeFromString<ArrayList<String>>(value)

    @TypeConverter
    fun fromColor(value : ColorWrapper) = value.c.value.toLong()
    @TypeConverter
    fun toColor(value: Long) = ColorWrapper(Color(value.toULong()))
}
@Database(entities = [
    BookData::class, BookQuery::class,
    PagerData::class,
    BufferData::class,
    WorkspaceData::class, WorkspaceQuery::class,
    LanguageData::class,
    WordData::class, WordQuery::class,
    RelationData::class, RelationQuery::class,
    WordRelation::class,
    SearchData::class,
    FormulaData::class, FormulaInnerData::class, FormulaQuery::class,
    NoteData::class
], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(): AppDatabase {
            val context = App.context
            return INSTANCE ?: synchronized(AppDatabase::class.java) {
                INSTANCE ?: Room.databaseBuilder(
                            context.applicationContext,
                            AppDatabase::class.java,"database"
                        ).build().also { runBlocking{ prepopulate(it)}; INSTANCE = it}
            }
        }

        private suspend fun prepopulate(db: AppDatabase) {
            if (db.relation().all().isEmpty() && db.word().all().isEmpty() && db.workspace().size() == 0) {
                db.relation().insert(RelationData(1, "Translation", false, ColorWrapper(Color.Blue)))
                db.relation().insert(RelationData(2, "Synonym", true, ColorWrapper(Color.Cyan)))
                db.formula().insert(FormulaInnerData(1, 0, 1, 1, 1))
                db.formula().insert(FormulaInnerData(2, 1, 2, 1, 1))
                db.formula().insert(FormulaData(1, 2, "TransSyn", ColorWrapper(Color.Green)))

                db.word().insert(WordData(1, "learn", "en"))
                db.note().insert(NoteData(1, "Arbitrary notes."))
                db.word().insert(WordData(2, "tadni", "jbo"))
                db.word().insert(WordData(3, "language", "en"))
                db.word().insert(WordData(4, "bangu", "jbo"))
                db.relation().insert(WordRelation(0, 1, 1, 2))
                db.relation().insert(WordRelation(0, 1, 3, 4))

                db.buffer().insert(BufferData(1, BufferType.SearchBook, 0))
                db.buffer().insert(BufferData(2, BufferType.Word, 4))
                db.buffer().insert(BufferData(3, BufferType.Word, 1))
                db.workspace().insert(WorkspaceData(1, "Default", arrayListOf(arrayListOf(1, 2), arrayListOf(3))))
            }
        }
    }
    abstract fun book(): BookDao
    abstract fun bookQuery(): BookQueryDao

    abstract fun formula(): FormulaDao
    abstract fun formulaQuery(): FormulaQueryDao

    abstract fun workspace(): WorkspaceDao
    abstract fun workspaceQuery(): WorkspaceQueryDao

    abstract fun relation(): RelationDao
    abstract fun relationQuery(): RelationQueryDao

    abstract fun word(): WordDao
    abstract fun wordQuery(): WordQueryDao

    abstract fun note(): NoteDao
    abstract fun pager(): PagerDao
    abstract fun buffer(): BufferDao
    abstract fun language(): LanguageDao
    abstract fun search(): SearchDao
}