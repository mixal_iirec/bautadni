package com.bautadni.data.lang

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query

@Entity
data class NoteData (
    @PrimaryKey var uid: Int,
    @ColumnInfo(name = "note") val note: String,
)
@Dao
interface NoteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(word: NoteData): Long

    @Query("select * from NoteData where uid = :uid")
    suspend fun byUid(uid: Int): NoteData?
}
