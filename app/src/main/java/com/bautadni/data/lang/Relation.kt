package com.bautadni.data.lang

import androidx.compose.ui.graphics.Color
import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.Companion.CASCADE
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import com.bautadni.App
import com.bautadni.data.AppDatabase
import com.bautadni.data.ColorWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

@Entity
data class RelationData(
    @PrimaryKey(autoGenerate = true) var uid: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "transitive") val transitive: Boolean,
    @ColumnInfo(name = "color") val color: ColorWrapper,
) {
    companion object {
        fun default(): RelationData = RelationData(0, "", false, ColorWrapper(Color.Green))
    }
}


@Entity(
    foreignKeys = [ForeignKey(
        entity = WordData::class,
        parentColumns = arrayOf("uid"),
        childColumns = arrayOf("first"),
        onDelete = CASCADE
    ), ForeignKey(
        entity = WordData::class,
        parentColumns = arrayOf("uid"),
        childColumns = arrayOf("second"),
        onDelete = CASCADE
    ), ForeignKey(
        entity = RelationData::class,
        parentColumns = arrayOf("uid"),
        childColumns = arrayOf("kind"),
        onDelete = CASCADE
    )]
)
data class WordRelation(
    @PrimaryKey(autoGenerate = true) var uid: Int,
    @ColumnInfo(name = "kind", index = true) val kindUid: Int,
    @ColumnInfo(name = "first", index = true) val first: Int,
    @ColumnInfo(name = "second", index = true) val second: Int,
) {
    companion object {
        suspend fun toggle(kind: Int, word1: WordData, word2: WordData) = withContext(context = Dispatchers.IO) {
            val database = AppDatabase.getInstance()
            val word1 = if (word1.uid != 0) word1 else database.word().byWordLang(word1.word, word1.lang)
            val word2 = if (word2.uid != 0) word2 else database.word().byWordLang(word2.word, word2.lang)
            WordRelation(0, kind, word1.uid, word2.uid).toggle(database)
        }
    }
    private suspend fun toggle(database: AppDatabase): Boolean {
        val find = database.relation().byWordUids(kindUid, first, second)
        find?.let {
            database.relation().deleteWordRelation(listOf(find.uid))
            return false
        }
        database.relation().insert(this)
        return true
    }
}

@Dao
interface RelationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(lang: RelationData): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(lang: WordRelation): Long

    @Query("delete from RelationData where uid in (:uids)")
    suspend fun delete(uids: List<Int>)

    @Query("delete from WordRelation where uid in (:uids)")
    suspend fun deleteWordRelation(uids: List<Int>)

    @Query("delete from WordRelation where (first in (:w1) and second in (:w2)) or (first in (:w2) and second in (:w1))")
    suspend fun deleteAllCombinations(w1: List<Int>, w2: List<Int>)

    @Query("select first from WordRelation where kind =:kind and second in (:w) union select second from WordRelation where kind =:kind and first in (:w)")
    suspend fun findRelatedWords(kind: Int, w: List<Int>): List<Int>

    @Query("select distinct kind from WordRelation where first =:uid or second =:uid")
    suspend fun kindsWhichContain(uid: Int): List<Int>

    @Query("select * from RelationData where uid=:uid ")
    suspend fun byUid(uid: Int): RelationData?

    @Query("select * from WordRelation where kind =:kindUid and first=:uid1 and second =:uid2 union select * from WordRelation where kind =:kindUid and first=:uid2 and second =:uid1")
    suspend fun byWordUids(kindUid: Int, uid1: Int, uid2: Int): WordRelation?

    // returns relations which contain the word specified by uid
    suspend fun whichContain(uid: Int): List<RelationData> {
        val database = AppDatabase.getInstance()
        return kindsWhichContain(uid).mapNotNull { database.relation().byUid(it) }
    }


    @Query("select * from RelationData order by name")
    suspend fun all(): List<RelationData>

    @Query("select * from RelationData order by name")
    fun flowAll(): Flow<List<RelationData>>

    @Query("select * from WordRelation where kind =:kind and ((first =:first and second =:second) or (first =:second and second =:first))")
    suspend fun byKindFirstSecondFallible(kind: Int, first: Int, second: Int): WordRelation?

    suspend fun byKindFirstSecond(kind: Int, first: Int, second: Int): WordRelation {
        byKindFirstSecondFallible(kind, first, second)?.let { return it }
        val rel = WordRelation(0, kind, first, second)
        rel.uid = insert(rel).toInt()
        return rel
    }

    @Query("select count(*) from WordRelation")
    fun flowRelationChange(): Flow<Int>
}


@Entity
class RelationQuery(
    @PrimaryKey(autoGenerate = true) var uid: Int = 0,
    @ColumnInfo(name = "name") val name: String,
) {
    companion object {
        fun all() = RelationQuery(0, "")
    }

    fun match(relation: RelationData): Boolean {
        if (name != "" && !name.toRegex().containsMatchIn(relation.name)) {
            return false
        }
        return true
    }

    suspend fun query(): List<RelationData> {
        val database = AppDatabase.getInstance()
        return database.relation().all().filter { match(it) }
    }

    suspend fun cloneDtb(database: AppDatabase): Int {
        uid = 0
        return database.relationQuery().insert(this).toInt()
    }
}

@Dao
interface RelationQueryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(query: RelationQuery): Long

    @Query("select * from RelationQuery where uid=:uid ")
    suspend fun byUid(uid: Int): RelationQuery?
}
