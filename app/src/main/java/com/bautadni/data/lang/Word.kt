package com.bautadni.data.lang

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import com.bautadni.data.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

@Entity
data class WordData (
    @PrimaryKey(autoGenerate = true) var uid: Int,
    @ColumnInfo(name = "word") val word: String,
    @ColumnInfo(name = "lang") val lang: String,
) {
    companion object {
        fun default() = WordData(0, "", "")
    }
}

@Dao
interface WordDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(word: WordData): Long

    @Query("delete from WordData where uid in (:uids)")
    suspend fun delete(uids: List<Int>)

    @Query("select * from WordData where uid = :uid")
    suspend fun byUid(uid: Int): WordData?

    @Query("select * from WordData where uid = :uid")
    fun flowByUid(uid: Int): Flow<WordData>

    @Query("select * from WordData where uid in (:uids)")
    suspend fun byUids(uids: List<Int>): List<WordData>

    @Query("select * from WordData order by word")
    suspend fun all(): List<WordData>

    @Query("select * from WordData where word = :word and lang = :lang")
    suspend fun byWordLangFallible(word: String, lang: String): WordData?

    suspend fun byWordLang(word: String, lang: String): WordData = withContext(context = Dispatchers.IO) {
        byWordLangFallible(word, lang)?.let { return@withContext it }
        val word = WordData(0, word, lang)
        word.uid = insert(word).toInt()
        word
    }
}

@Entity
class WordQuery(
    @PrimaryKey(autoGenerate = true) var uid: Int = 0,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "lang") val lang: String,
) {
    companion object {
        fun all() = WordQuery (0, "", "")
    }

    fun match(word: WordData): Boolean {
        if (name != "" && !name.toRegex().containsMatchIn(word.word)) {
            return false
        }
        if (lang != "" && !lang.toRegex().containsMatchIn(word.lang)) {
            return false
        }
        return true
    }

    suspend fun query(): List<WordData> {
        val database = AppDatabase.getInstance()
        return database.word().all().filter {match(it)}
    }

    suspend fun cloneDtb(database: AppDatabase): Int {
        uid = 0
        return database.wordQuery().insert(this).toInt()
    }
}

@Dao
interface WordQueryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(query: WordQuery): Long

    @Query("select * from WordQuery where uid=:uid ")
    suspend fun byUid(uid: Int): WordQuery?
}
