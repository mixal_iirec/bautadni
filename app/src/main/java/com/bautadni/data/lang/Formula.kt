package com.bautadni.data.lang

import android.util.Log
import androidx.compose.ui.graphics.Color
import com.bautadni.data.AppDatabase
import com.bautadni.data.ColorWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class Formula(val data: FormulaData) {
    val inners: ArrayList<FormulaInnerData> = arrayListOf()

    companion object {
        fun simpleRelation (rel: RelationData): Formula {
            val f = Formula(FormulaData(0, 0, rel.name, ColorWrapper(rel.color.c)))
            val min = if (rel.transitive) 0 else 1
            val max = if (rel.transitive) Int.MAX_VALUE else 1
            f.inners.add(FormulaInnerData(0, 0, rel.uid, min, max))
            return f
        }

        fun default() = Formula(FormulaData(0, 0, "", ColorWrapper(Color.Green)))
    }

    fun memoryCopy(): Formula {
        val copy = Formula(data.copy())

        for (inner in inners) {
            copy.inners.add(inner.copy())
        }

        return copy
    }

    suspend fun load() = withContext(context = Dispatchers.IO) {
        inners.clear()

        val database = AppDatabase.getInstance()
        var nextUid = data.innerUid
        while (nextUid > 0) {
            database.formula().byUidInner(nextUid)?.let { inner->
                inners.add(0, inner)
                nextUid = inner.nextFormulaUid
            } ?: {
                if (inners.isEmpty()) {
                    data.innerUid = 0
                } else {
                    inners[0].nextFormulaUid = 0
                }
                nextUid = 0
            }
        }
    }

    fun clearUids() {
        data.uid = 0
        inners.forEach {it.uid = 0}
    }

    suspend fun save() = withContext(context = Dispatchers.IO) {
        val database = AppDatabase.getInstance()

        for (i in 0..<inners.size) {
            inners[i].nextFormulaUid = inners.getOrNull(i-1)?.uid ?: 0

            inners[i].uid = database.formula().insert(inners[i]).toInt()
        }

        data.innerUid = inners.getOrNull(inners.lastIndex)?.uid ?: 0
        data.uid = database.formula().insert(data).toInt()
    }

    private suspend fun applyInnerOnce(database: AppDatabase, inner: FormulaInnerData, words: List<Int>): List<Int> {
        return database.relation().findRelatedWords(inner.relKindUid, words)
    }

    private suspend fun applyInner(database: AppDatabase, inner: FormulaInnerData, words: Set<Int>): Set<Int> {
        var current = words

        var encountered = words
        var ret = if (inner.minCount == 0) words else setOf()

        for (i in 1..inner.maxCount) {
            val next: Set<Int> = applyInnerOnce(database, inner, current.toList()) subtract encountered
            if (next.isEmpty()) break

            encountered = encountered.union(next)
            if (i >= inner.minCount) {
                ret = ret.union(next)
            }

            current = next
        }

        return ret
    }

    private suspend fun apply(words: Set<Int>): Set<Int> {
        val database = AppDatabase.getInstance()
        return inners.fold(words) {words, inner ->
            applyInner(database, inner, words)
        }
    }

    suspend fun apply(word: Int): List<WordData> = withContext(context = Dispatchers.IO) {
        val database = AppDatabase.getInstance()
        val words = apply(setOf(word))
        database.word().byUids(ArrayList(words))
    }

    suspend fun apply(word: String, lang: String): List<WordData> = withContext(context = Dispatchers.IO) {
        val database = AppDatabase.getInstance()
        val word = database.word().byWordLang(word, lang)
        apply(word.uid)
    }

    fun applyFlow(word: Int): Flow<List<WordData>> {
        val database = AppDatabase.getInstance()
        return database.relation().flowRelationChange().map {
            val words = apply(setOf(word))
            database.word().byUids(ArrayList(words))
        }
    }
}