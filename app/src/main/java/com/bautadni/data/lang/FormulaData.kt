package com.bautadni.data.lang

import androidx.compose.ui.graphics.Color
import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import com.bautadni.data.AppDatabase
import com.bautadni.data.ColorWrapper
import kotlinx.coroutines.flow.Flow

@Entity
data class FormulaInnerData (
    @PrimaryKey(autoGenerate = true) var uid: Int,
    @ColumnInfo(name = "next") var nextFormulaUid: Int,
    @ColumnInfo(name = "rel") val relKindUid: Int,
    @ColumnInfo(name = "min") val minCount: Int,
    @ColumnInfo(name = "max") val maxCount: Int,
)

@Entity
data class FormulaData (
    @PrimaryKey(autoGenerate = true) var uid: Int,
    @ColumnInfo(name = "inner") var innerUid: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "color") val color: ColorWrapper,
) {
    companion object {
        fun default() = FormulaData(0, 0, "", ColorWrapper(Color.Green))
    }


}

@Dao
interface FormulaDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(word: FormulaData): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(word: FormulaInnerData): Long

    @Query("delete from FormulaData where uid in (:uids)")
    suspend fun delete(uids: List<Int>)

    @Query("select * from FormulaData where uid=:uid ")
    suspend fun byUid(uid: Int): FormulaData?

    @Query("select * from FormulaInnerData where uid=:uid ")
    suspend fun byUidInner(uid: Int): FormulaInnerData?

    @Query("select * from FormulaData order by name")
    suspend fun all(): List<FormulaData>

    @Query("select * from FormulaData order by name")
    fun flowAll(): Flow<List<FormulaData>>
}

@Entity
class FormulaQuery(
    @PrimaryKey(autoGenerate = true) var uid: Int = 0,
    @ColumnInfo(name = "name") val name: String,
) {
    companion object {
        fun all() = FormulaQuery (0, "")
    }

    fun match(formula: FormulaData): Boolean {
        if (name != "" && !name.toRegex().containsMatchIn(formula.name)) {
            return false
        }
        return true
    }

    suspend fun query(): List<FormulaData> {
        val database = AppDatabase.getInstance()
        return database.formula().all().filter {match(it)}
    }

    suspend fun cloneDtb(database: AppDatabase): Int {
        uid = 0
        return database.formulaQuery().insert(this).toInt()
    }
}

@Dao
interface FormulaQueryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(query: FormulaQuery): Long

    @Query("select * from FormulaQuery where uid=:uid ")
    suspend fun byUid(uid: Int): FormulaQuery?
}
