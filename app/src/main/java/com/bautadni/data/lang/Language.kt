package com.bautadni.data.lang

import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.PrimaryKey
import androidx.room.Query

@Entity
data class LanguageData (
    @PrimaryKey val name: String,
)
@Dao
interface LanguageDao {
    @Insert
    suspend fun insert(vararg lang: LanguageData)

    @Query("select * from LanguageData")
    suspend fun all(): List<LanguageData>
}