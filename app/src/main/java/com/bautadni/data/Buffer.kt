package com.bautadni.data

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
enum class BufferType {
    Pager,
    FormulaEditor,
    Word,
    Settings,
    Invalid,
    SearchBook,
    SearchWord,
    SearchRelation,
    SearchWorkSpace,
    SearchFormula,
}
@Entity
data class BufferData (
    @PrimaryKey(autoGenerate = true) var uid: Int = 0,
    @ColumnInfo(name = "type") val type: BufferType,
    @ColumnInfo(name = "type_uid") var typeUid: Int,
)
{
    companion object{
        fun unnull(data: BufferData?): BufferData{
            return data ?: BufferData(0, BufferType.Invalid, 0)
        }
    }
    suspend fun cloneDtb(database: AppDatabase): Int {
        uid = 0
        typeUid = when (type) {
            BufferType.Pager -> database.pager().byUid(typeUid)?.cloneDtb(database) ?: 0
            BufferType.FormulaEditor -> typeUid
            BufferType.Word -> typeUid
            BufferType.Settings -> 0
            BufferType.Invalid -> 0

            BufferType.SearchBook,
            BufferType.SearchWord,
            BufferType.SearchRelation,
            BufferType.SearchFormula,
            BufferType.SearchWorkSpace,
            -> database.search().byUid(typeUid)?.cloneDtb(database, type) ?: 0
        }
        return database.buffer().insert(this).toInt()
    }
}

@Dao
interface BufferDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(buffer: BufferData): Long
    @Insert
    suspend fun insert(vararg buffer: BufferData)

    @Query("select * from BufferData where uid=:uid ")
    fun byUid(uid: Int): BufferData?
}
